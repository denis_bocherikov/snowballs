using UnityEngine;

    public class BaseEnemyPositionBehaviour : MonoBehaviour, IEnemyPosition
    {
        public virtual IPlayerState GetEnemyOnPositionState(BoyModelBehaviour modelBehaviour, ITarget target, IPlayer player, ToolboxBehaviour toolbox)
        {
            return null;
        }
    }