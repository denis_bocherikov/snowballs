
using UnityEngine;

public class BaseEnemyRespawnBehaviour : MonoBehaviour, IRespawn
{
    public virtual IPlayerState GetPlayerRespawnState(ToolboxBehaviour toolbox, IPlayer player, ITarget target)
    {
        return null;
    }
}
