﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class BasePositionCollectionBehaviour : MonoBehaviour
{
    public int EnemiesCount;
    public List<BaseEnemyPositionBehaviour> Positions;
    public List<BaseEnemyRespawnBehaviour> Respawns;
    public HeroAttackPositionBehaviour HeroAttackPosition;
    public bool IsNeedShowRotateTip;
    public List<FireplaceBehaviour> Fireplaces;

    public abstract IPlayerState GetPreActionState(ToolboxBehaviour toolbox, HeroBehaviour hero);
    public abstract IPlayerState GetAfterActionState(ToolboxBehaviour toolbox, HeroBehaviour hero);
}
