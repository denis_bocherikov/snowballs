﻿
using System;
using UnityEngine;

public class BoyBehaviour : MonoBehaviour
{
    public float AttackSpeed;
    public event SimpleActionDelegate OnThrowStartDelegate;
    public event SimpleActionDelegate OnThrowEndDelegate;
    public event SimpleActionDelegate OnJumpComplete;
    public event SimpleActionDelegate OnDieEndDelegate;
    public event SimpleActionDelegate OnHalfJumpDelegate;

    public GameObject Stars;
    
    public GameObject UsualFace;
    public GameObject VictoryFace;
    public GameObject DefeatFace;
    
    private Animator _animator { get; set; }

    public void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Start ()
    {
        
        Idle();
    }

    public void AvoidSnowball()
    {
        _animator.speed = 1;
        _animator.Play("Avoid");
    }

    public void Attack()
    {
        SetUsualFace();
        _animator.speed = AttackSpeed;
        _animator.Play("Weapon Attack");
    }

    public void Victory()
    {
        if (Stars != null)
        {
            Stars.SetActive(false);
        }
        
        SetVictoryFace();
        _animator.speed = 1;
        _animator.Play("victory");
    }

    public void Die()
    {
        _animator.speed = 1;
        Stars.SetActive(false);
        _animator.Play("Die");
    }

    public void Defeat()
    {
        SetDefeatFace();
        _animator.speed = 1;
        _animator.Play("weapon_idle1");
    }

    public void Jump()
    {
        SetUsualFace();
        _animator.speed = 1;
        _animator.Play("Jump");
    }

    public void MakeSnowball()
    {
        _animator.speed = 1;
        _animator.Play("MakeSnowball");
    }

    public void Idle()
    {
        SetUsualFace();
        _animator.speed = 1;
        _animator.Play("idle");
    }
    

    public void Run()
    {
        SetUsualFace();
        _animator = GetComponent<Animator>();
        _animator.speed = 1;
        _animator.Play("run");
    }

    private void SetUsualFace()
    {
        if (UsualFace != null && VictoryFace != null)
        {
            UsualFace.SetActive(true);
            VictoryFace.SetActive(false);
            DefeatFace.SetActive(false);
        }
    }

    private void SetVictoryFace()
    {
        if (UsualFace != null && VictoryFace != null)
        {
            UsualFace.SetActive(false);
            VictoryFace.SetActive(true);
            DefeatFace.SetActive(false);
        }
    }
    private void SetDefeatFace()
    {
        if (UsualFace != null && VictoryFace != null)
        {
            UsualFace.SetActive(false);
            VictoryFace.SetActive(false);
            DefeatFace.SetActive(true);
        }
    }

    private void OnThrowMoveCompleted()
    {
        if (OnThrowEndDelegate != null)
        {
            OnThrowEndDelegate.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnDestroy()
    {
        OnThrowEndDelegate = null;
        OnJumpComplete = null;
    }

    private void ArrivedToPosition()
    {
        if (OnJumpComplete != null)
        {
            OnJumpComplete.Invoke(this, EventArgs.Empty);
        }
    }

    private void ThrowSnowball()
    {
        if (OnThrowStartDelegate != null)
        {
            OnThrowStartDelegate.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnDieEnd()
    {
        if (OnDieEndDelegate != null)
        {
            OnDieEndDelegate.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnHalfJumpComplete()
    {
        if (OnHalfJumpDelegate != null)
        {
            OnHalfJumpDelegate.Invoke(this, EventArgs.Empty);
        }
    }
    
}

public delegate void SimpleActionDelegate(object sender, EventArgs args);

public delegate void DamageDelegate(float value);
