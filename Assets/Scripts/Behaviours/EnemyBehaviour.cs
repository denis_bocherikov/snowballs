﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class EnemyBehaviour : PlayerBehaviour, IPlayer, IAttackingPlayer, ITarget
{
    public GameObject PointStartPosition;
    public IPlayerState State { get; set; }
    public bool IsCanAttack { get; private set; }
    
    private IPlayerState _mainState;
    public IEnemyPosition AttackPosition;
    public EnemyKilledOnPositionDelegate OnDie;
    private HeroProgressBehaviour _heroProgress;
    public DamageDelegate OnDamage;

    public bool IsCanBeAttacked
    {
        get
        {
            if (State != null)
            {
                return IsAlive && State.IsCanBeAttacked;
            }
            return true;
        }
    }

    public void Init(IPlayerState state, ToolboxBehaviour toolbox, IEnemyPosition position, ITarget target, HeroProgressBehaviour heroProgress, bool isCanAttack)
    {
        _heroProgress = heroProgress;
        IsAlive = true;
        State = state;
        State.OnStateError += OnStateError;
        _mainState = state;
        _toolbox = toolbox;
        var throwBehaviour = GetComponent<SnowballThrowBehaviour>();
        throwBehaviour.Spline = _toolbox.ThrowSpline;
        var collisionBehaviour = GetComponent<PlayerCollisionBehaviour>();
        collisionBehaviour.BoyModelBehaviour = BoyModelBehaviour;
        //throwBehaviour.Rejection = BoyModelBehaviour.Rejection;
        AttackPosition = position;
        Boy.OnDieEndDelegate += (sender, args) =>
        {
            gameObject.SetActive(false);
        };
        Boy.AttackSpeed = BoyModelBehaviour.AttackSpeed;
        Target = target;
        IsCanAttack = isCanAttack;
    }
    public override void Die()
    {
        State.Stop();
        Boy.Die();
        IsAlive = false;
        var collider = GetComponent<CapsuleCollider>();
        collider.enabled = false;
        if (OnDie != null)
        {
            OnDie.Invoke(this, AttackPosition);
        }
        Destroy(BoyModelBehaviour.gameObject);
    }

    public void Damage(float value)
    {
        var pointLabel = Instantiate(_toolbox.Point);
        pointLabel.transform.SetParent(_toolbox.SplashesCanvas.transform, false);
        var text = pointLabel.GetComponent<Text>();
        text.text = $"+{(int) value}";
        pointLabel.gameObject.SetActive(true);
        pointLabel.transform.position = _toolbox.Camera.WorldToScreenPoint(PointStartPosition.transform.position);
        pointLabel.MoveTo(pointLabel.transform.position + new Vector3(10, 20));
        if (OnDamage != null)
        {
            OnDamage.Invoke(value);
        }
    }

    public void Stun()
    {
        State.Stun();
    }

    public void Act()
    {
        State.Act(this);
    }
    void Update()
    {
        State.Update(this);
    }

    public void Victory()
    {
        State.Stop();
        Boy.Victory();
    }

    public void StartAttack()
    {
        IsCanAttack = true;
    }
    

    private void OnStateError(object sender, EventArgs e)
    {
        Die();
    }

    public Vector3 HandAttackPosition => SnowballPosition.transform.position;

    public Vector3 TargetPosition => transform.position;
}
