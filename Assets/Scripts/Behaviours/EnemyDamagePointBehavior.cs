﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamagePointBehavior : MonoBehaviour
{
    private bool _isMoving;
    private Vector3 _endPosition;
    private float _liveTime = 1f;
    public void MoveTo(Vector3 endPosition)
    {
        _endPosition = endPosition;
        _isMoving = true;
    }

    private void Update()
    {
        if (_isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, _endPosition, Time.deltaTime * 30f);
            _liveTime -= Time.deltaTime;
            if (_liveTime < 0)
            {
               Destroy(gameObject);
            }
        }
    }
}
