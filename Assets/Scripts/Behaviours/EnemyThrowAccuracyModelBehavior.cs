﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LooseMoment
{
    Start=0,
    Middle,
    End
}
public class EnemyThrowAccuracyModelBehavior : MonoBehaviour
{
    public BoyModelBehaviour EnemyModel;
    public HeroProgressBehaviour HeroProgress;
    private int _currentLevelEnemiesCount { get; set; } //Should be sum of all subleveles
    private LooseMoment _looseAt = LooseMoment.Middle;
    private bool _isShouldLoose { get; set; }
    
    private int _lastLooseLevel;
    private string _lastLooseLevelKey = "lastLooseLevel";
    private string _looseCount = "looseCount";
    private int _levelDiff;
    private HeroProgress _heroProgress;
    private int _enemyDieCount; 

    public void Init(bool isShouldLoose, LooseMoment looseMoment, int currentLevelEnemiesCount)
    {
        _isShouldLoose = isShouldLoose;
        _looseAt = looseMoment;
        _currentLevelEnemiesCount = currentLevelEnemiesCount;
        Random.InitState(17);
        _lastLooseLevel = PlayerPrefs.GetInt(_lastLooseLevelKey, -1);
        _heroProgress = HeroProgress.GetCurrentProgress();
        _levelDiff = Random.Range(3, 4);
    }

    public float GetRejection()
    {
        if ((_isShouldLoose && _lastLooseLevel != _heroProgress.CurrentLevel || _heroProgress.CurrentLevel - _lastLooseLevel >= _levelDiff) && IsTimeToLoose())
        {
            return 0.1f;
        }

        return EnemyModel.Rejection;
    }

    public void EnemyDied()
    {
        _enemyDieCount++;
    }

    public void ResetEnemyDiesCount()
    {
        _enemyDieCount = 0;
    }

    public void HeroDied()
    {
        PlayerPrefs.SetInt(_lastLooseLevelKey, _heroProgress.CurrentLevel);
        var lastLooseCount = PlayerPrefs.GetInt(_looseCount, 0);
        lastLooseCount++;
        PlayerPrefs.SetInt(_looseCount, lastLooseCount);
    }

    public void HeroWin()
    {
        PlayerPrefs.SetInt(_looseCount, 0);
    }
    

    public float GetAdditionalHealth(float originalHealth)
    {
        var lastLooseCount = PlayerPrefs.GetInt(_looseCount, 0);
        if (lastLooseCount == 0)
        {
            return originalHealth;
        }

        return originalHealth + originalHealth * 0.3f * lastLooseCount;
    }
    

    private bool IsTimeToLoose()
    {
        int startCount = 0;
        switch (_looseAt)
        {
            case LooseMoment.Start: startCount = 3;
                break;
                case LooseMoment.Middle: startCount = _currentLevelEnemiesCount / 3;
                    break;
                case LooseMoment.End: startCount = _currentLevelEnemiesCount / 3 * 2; break;
        }

        return _enemyDieCount >= startCount;
    }
}
