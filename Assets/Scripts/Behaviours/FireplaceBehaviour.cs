﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireplaceBehaviour : MonoBehaviour
{
    public ParticleSystem Red;
    public ParticleSystem Yellow;
    public ParticleSystem Smoke;
    public GameObject Light;

    public void TurnOn()
    {
        var lensFlare = Light.GetComponent<LensFlare>();
        lensFlare.enabled = true;
        var light = Light.GetComponent<Light>();
        light.enabled = true;
        Red.Play();
        Yellow.Play();
        Smoke.Play();
    }

    public void TurnOff()
    {
        Smoke.Stop();
        Yellow.Stop();
        Red.Stop();
        var lensFlare = Light.GetComponent<LensFlare>();
        lensFlare.enabled = false;
        var light = Light.GetComponent<Light>();
        light.enabled = false;
    }
}
