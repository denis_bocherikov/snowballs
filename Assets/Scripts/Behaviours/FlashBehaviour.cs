﻿using System;
using UnityEngine;

public class FlashBehaviour : MonoBehaviour
{
    public SimpleActionDelegate OnFlashTurnedOnEvent;
    public SimpleActionDelegate OnFlashTornedOffEvent;

    public void FlashOn()
    {
        var flashAnimator = GetComponent<Animator>();
        flashAnimator.Play("FlashOn");
    }

    public void FlashOff()
    {
        var flashAnimator = GetComponent<Animator>();
        flashAnimator.Play("FlashOff");
    }

    private void OnFlashTurnedOn()
    {
        OnFlashTurnedOnEvent?.Invoke(this, EventArgs.Empty);
    }

    private void OnFlashTurnedOff()
    {
        OnFlashTornedOffEvent?.Invoke(this, EventArgs.Empty);
    }
}