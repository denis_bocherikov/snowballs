﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GameDirectorBehaviour : MonoBehaviour
{
    private LevelBehaviour _currentLevel;
    private LocationBehaviour _currentLocation;
    private int _currentStartPositionFillIndex;
    private BasePositionCollectionBehaviour _currentSubLevel;
    private bool _isHeroDefeted;
    private bool _isLevelCompleted;
    private bool _isLevelStarted;
    private float _levelCompeleteDelay;
    private readonly float _levelCompleteDelayValue = 2f;
    private int _nextLevelIndex;
    private int _nextSubLevelIndex;
    private bool _isHeroArrivedToPosition;
    public Camera Camera;

    public List<EnemyBehaviour> Enemies = new List<EnemyBehaviour>();
    public BoyModelBehaviour EnemyBoyModelBehaviour;
    public EnemyBehaviour EnemyPrefab;
    public FlashBehaviour Flash;
    public BoyModelBehaviour HeroBoyModelBehaviour;

    public HeroDamageCanvasBehaviour HeroDamageBehaviour;
    public HeroProgressBehaviour HeroModelService;
    public HeroBehaviour HeroPrefab;
    public GameObject HeroStartPosition;

    public LevelLoaderBehaviour LevelLoader;

    public GameObject Menu;
    public GameObject SnowballSmash;
    public FireplaceBehaviour StartFirePlace;
    public ToolboxBehaviour Toolbox;
    public Text TouchToPlayTip;
    private HeroBehaviour _hero { get; set; }
    private TargetsContainer _targets { get; } = new TargetsContainer();
    private float _currentLevelPoints;

    private void Start()
    {
        
        Toolbox.SplashesCanvas = SnowballSmash;
        Flash.OnFlashTurnedOnEvent += OnFlashTurnedOn;
        Flash.OnFlashTornedOffEvent += OnFlashTurnedOff;
        FlashOff();
        _currentLocation = transform.parent.GetComponent<LocationBehaviour>();
        
        _hero = Instantiate(HeroPrefab);
        Toolbox.SnowPile.transform.SetParent(_hero.transform.parent);
        _hero.OnDie += OnHeroDie;
        _hero.BoyModelBehaviour = Instantiate(HeroBoyModelBehaviour);
        var heroProgressModel = HeroModelService.GetCurrentProgress();
        _hero.Init(Toolbox, this, heroProgressModel, _targets, Toolbox.EnemyAccuracy.GetAdditionalHealth(18));
        _hero.OnArriveToPosition += OnHeroArriveToPosition;
        _hero.transform.rotation = HeroStartPosition.transform.rotation;
        Toolbox.Camera = Camera;
        //Toolbox.CameraOffsetPosition = CameraOffset;
        LoadLevel();
        TurnOnInitialLights();
        //_hero.transform.position = HeroStartPosition.transform.position;


        Toolbox.TargetsContainer = _targets;

        CalculateOfflineEarn();
    }

    private void OnHeroArriveToPosition(object sender, EventArgs e)
    {
        var heroPosition = _hero.transform.position;
        Toolbox.SnowPile.SetActive(true);
        Toolbox.SnowPile.transform.position = new Vector3(heroPosition.x,
            heroPosition.y + Toolbox.MinSnowPilePositionYOffset - 0.14f, heroPosition.z); 
        _isHeroArrivedToPosition = true;
        foreach (var enemy in Enemies)
        {
            enemy.StartAttack();
        }
    }

    private void OnHeroDie(object sender, EventArgs e)
    {
        if (_isLevelStarted)
        {
            HeroModelService.AddPoint(_currentLevelPoints);
            HeroModelService.UpdateMenuState();
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, Application.version,
                $"level {(LevelService.Instance.GetLevelModel().LevelIndex + 1).ToString("00000")}");
            _levelCompeleteDelay = _levelCompleteDelayValue;
            SnowballSmash.SetActive(false);
            Toolbox.Fail.gameObject.SetActive(true);
            Toolbox.EnemyAccuracy.HeroDied();
            Menu.SetActive(true);
            _isHeroDefeted = true;
            _isLevelStarted = false;
            foreach (var enemy in Enemies)
            {
                enemy.Victory();
            }
        }
    }


    private void TurnOnInitialLights()
    {
        if (_nextLevelIndex == 0)
        {
            if (StartFirePlace != null)
            {
                StartFirePlace.TurnOn();
            }
        }
        else
        {
            foreach (var fireplace in _currentLocation.Levels[_nextLevelIndex - 1].SubLevels.Last().Fireplaces)
            {
                fireplace.TurnOn();
            }
        }
    }

    private void CalculateOfflineEarn()
    {
        var lastStartString = PlayerPrefs.GetString("lastStart");
        if (string.IsNullOrEmpty(lastStartString))
        {
            lastStartString = DateTime.UtcNow.ToString();
            PlayerPrefs.SetString("lastStart", lastStartString);
            return;
        }

        var lastStartTime = DateTime.SpecifyKind(DateTime.Parse(lastStartString), DateTimeKind.Utc);
        if (DateTime.UtcNow < lastStartTime)
        {
            return;
        }

        var timeSpan = DateTime.UtcNow - lastStartTime;
        var progressModel = HeroModelService.GetCurrentProgress();
        var earnedMoney = timeSpan.TotalMinutes * (progressModel.OfflineEarn / 60);
        HeroModelService.AddPoint((float) earnedMoney);

        lastStartString = DateTime.UtcNow.ToString();
        PlayerPrefs.SetString("lastStart", lastStartString);
    }

    public void FlashOn()
    {
        Flash.gameObject.SetActive(true);
        Flash.FlashOn();
    }

    public void FlashOff()
    {
        Flash.FlashOff();
    }

    private void OnFlashTurnedOn(object sender, EventArgs e)
    {
        if (_isHeroDefeted)
        {
            _hero.OnDie -= OnHeroDie;

            foreach (var enemy in Enemies)
            {
                Destroy(enemy.gameObject);
            }

            LevelLoader.LoadLevel();
        }
        else
        {
            OnPlayStart();
        }
    }

    private void OnFlashTurnedOff(object sender, EventArgs e)
    {
        Flash.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (_levelCompeleteDelay > 0)
        {
            _levelCompeleteDelay -= Time.deltaTime;
        }

        if (!_isLevelStarted)
        {
            if (_levelCompeleteDelay <= 0 && !Toolbox.TouchToPlayTip.IsActive() && Toolbox.TouchToPlayTip != null && Toolbox.TouchToPlayTip.gameObject != null)
            {
                Toolbox.TouchToPlayTip.gameObject.SetActive(true);
            }
            
            Toolbox.Camera.transform.position = Vector3.MoveTowards(Toolbox.Camera.transform.position,
                _hero.transform.TransformPoint(Toolbox.VictoryCameraOffsetPosition),
                Time.deltaTime * 0.2f);
            Toolbox.Camera.transform.LookAt(_hero.transform);
        }
    }

    private void LateUpdate()
    {
        if (!_isLevelStarted && _levelCompeleteDelay <= 0)
        {
            if (Input.touchCount > 0 && Input.touches[0].position.y > TouchToPlayTip.transform.position.y - 40)
            {
                _isLevelStarted = true;
                if (_currentLocation.Levels.Count <= _nextLevelIndex || _isHeroDefeted)
                {
                    Toolbox.Fail.gameObject.SetActive(false);
                    FlashOn();
                }
                else
                {
                    Toolbox.Confetti.Stop();
                    OnPlayStart();
                }
            }
        }
    }

    public void OnEnemyDie(EnemyBehaviour enemy, IEnemyPosition position)
    {
        Toolbox.EnemyAccuracy.EnemyDied();
        _targets.Remove(enemy);
        Enemies.Remove(enemy);
        if (_currentSubLevel.EnemiesCount > 0)
        {
            PositionWasFreed(position);
        }
        else
        {
            if (Enemies.Count == 0)
            {
                HeroModelService.AddPoint(_currentLevelPoints);
                Toolbox.EnemyAccuracy.HeroWin();
                LoadSubLevel();
            }
        }
    }

    public void PositionWasFreed(IEnemyPosition position, int index = 0)
    {
        var respawnsList = _currentSubLevel.Respawns.Select(a => a.GetComponent<IRespawn>());
        var newEnemy = Instantiate(EnemyPrefab);
        newEnemy.OnDie += OnEnemyDie;
        newEnemy.OnDamage += OnEnemyDamage;
        newEnemy.BoyModelBehaviour = Instantiate(EnemyBoyModelBehaviour);
        newEnemy.BoyModelBehaviour.Health *= LevelService.Instance.GetAdditionalEnemyModelCoefficient();
        var respawn = RandomHelper<IRespawn>.GetRandomValue(respawnsList);
        newEnemy.transform.position = respawn.transform.position;
            //CalculateRespawnPosition(respawn.transform.position, _hero.transform.position, index);
        newEnemy.transform.LookAt(_hero.transform);
//        var enemyAgent = newEnemy.GetComponent<NavMeshAgent>();
//        enemyAgent.enabled = true;
        var sequence = WalkingEnemyStateBuilder.Instance.GetSequenceState();
        var respawnState = respawn.GetPlayerRespawnState(Toolbox, newEnemy, _hero);
        if (respawnState != null)
        {
            sequence.SetNextState(respawnState);
        }

        var goToPosition = WalkingEnemyStateBuilder.Instance.GetGoToPositionState(position.transform.position, _hero);
        sequence.SetNextState(goToPosition);
        var positionState = position.GetEnemyOnPositionState(newEnemy.BoyModelBehaviour, _hero, newEnemy, Toolbox);
        sequence.SetNextState(positionState);
        newEnemy.Init(sequence, Toolbox, position, _hero, HeroModelService, _isHeroArrivedToPosition);

        Enemies.RemoveAll(a => !a.IsAlive);
        Enemies.Add(newEnemy);
        _targets.Add(newEnemy);
        newEnemy.Act();
        _currentSubLevel.EnemiesCount--;
        if (_isHeroDefeted)
        {
            newEnemy.Victory();
        }
    }

    private void OnEnemyDamage(float value)
    {
        _currentLevelPoints += value;
    }

    private Vector3 CalculateRespawnPosition(Vector3 respawnPosition, Vector3 heroPosition, int index)
    {
        var position = index == 0 ? respawnPosition : respawnPosition + index * (respawnPosition - heroPosition).normalized * 0.7f;
        if (index == 0)
        {
            return respawnPosition;
        }

        NavMeshHit hit;
        if (NavMesh.SamplePosition(position, out hit, 5, 1))
        {
            return hit.position;
        }

        return respawnPosition;
    }

    private void OnDestroy()
    {
        if (_hero != null && _hero.gameObject != null)
        {
            Destroy(_hero.gameObject);
        }

        _hero = null;
        LevelLoader = null;
    }

    private void OnPlayStart()
    {
        _currentLevelPoints = 0;
        var heroProgressModel = HeroModelService.GetCurrentProgress();
        _hero.Init(Toolbox, this, heroProgressModel, _targets, Toolbox.EnemyAccuracy.GetAdditionalHealth(18));
        
        var heroThrowBehaviour = _hero.GetComponent<SnowballThrowBehaviour>();
        heroThrowBehaviour.Rejection = heroProgressModel.Rejection;
        
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Application.version,
            $"level {(LevelService.Instance.GetLevelModel().LevelIndex + 1).ToString("00000")}");
        _isHeroArrivedToPosition = false;
        Toolbox.TouchToPlayTip.gameObject.SetActive(false);
        Menu.SetActive(false);
        Toolbox.Fail.gameObject.SetActive(false);
        SnowballSmash.SetActive(true);
        Toolbox.EnemyAccuracy.ResetEnemyDiesCount();
        if (_nextSubLevelIndex > 0)
        {
            foreach (var fireplace in _currentLevel.SubLevels[_nextSubLevelIndex - 1].Fireplaces)
            {
                fireplace.TurnOff();
            }
        }

        if (_nextLevelIndex > 0)
        {
            foreach (var fireplace in _currentLocation.Levels[_nextLevelIndex - 1].SubLevels.Last().Fireplaces)
            {
                fireplace.TurnOff();
            }
        }

        LoadNextLevel();
    }

    private void LoadLevel() // load only camera and hero position on game director start
    {
        var levelModel = LevelService.Instance.GetLevelModel();
        _nextLevelIndex = levelModel.LocationSubLevel;
        if (_nextLevelIndex == 0)
        {
            _hero.transform.position = HeroStartPosition.transform.position;
        }
        else
        {
            _hero.transform.position = _currentLocation.Levels[_nextLevelIndex - 1].SubLevels.Last().HeroAttackPosition
                .transform.position;
            Toolbox.Camera.transform.position = 
                _hero.transform.TransformPoint(Toolbox.VictoryCameraOffsetPosition) + new Vector3(0, 0.25f, 0);
            Toolbox.Camera.transform.LookAt(_hero.transform);
        }
        var agent = _hero.GetComponent<NavMeshAgent>();
        agent.enabled = true;
    }

    private void LoadNextLevel()
    {
        if (_nextLevelIndex < _currentLocation.Levels.Count)
        {
            _currentLevel = _currentLocation.Levels[_nextLevelIndex];
            Toolbox.EnemyAccuracy.Init(_currentLevel.IsShouldLoose, _currentLevel.LooseAt,
                _currentLevel.SubLevels.Sum(a => a.EnemiesCount));
            LoadSubLevel();
            Toolbox.LevelNumberLabel.gameObject.SetActive(false);
            Toolbox.CompletedLabel.gameObject.SetActive(false);
        }
        else
        {
            LevelService.Instance.CompleteLocation();
            LevelLoader.LoadLevel();
        }
    }


    private void LoadSubLevel()
    {
        _isHeroArrivedToPosition = false;
        if (_nextSubLevelIndex == 0)
        {
            StartFirePlace?.TurnOff();
        }

        if (_nextSubLevelIndex < _currentLevel.SubLevels.Count)
        {
            _currentSubLevel = _currentLevel.SubLevels[_nextSubLevelIndex];
            foreach (var fireplace in _currentSubLevel.Fireplaces)
            {
                fireplace.TurnOn();
            }

            var index = 0;
            foreach (var position in _currentSubLevel.Positions)
            {
                PositionWasFreed(position, index);
                index++;
            }

            _hero.SetState(_currentSubLevel.GetPreActionState(Toolbox, _hero));

            _nextSubLevelIndex++;
        }
        else
        {
            if (!_hero.IsDefeated)
            {
                var levelModel = LevelService.Instance.CompleteLocationSubLevel();
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Application.version,
                    $"level {LevelService.Instance.GetLevelModel().LevelIndex.ToString("00000")}");
                HeroModelService.UpdateMenuState();
                _nextLevelIndex++;
                _nextSubLevelIndex = 0;
                SnowballSmash.SetActive(false);
                Menu.SetActive(true);
                _isLevelCompleted = true;
                _levelCompeleteDelay = _levelCompleteDelayValue;
                _isLevelStarted = false;
                Toolbox.LevelNumberLabel.text = $"Level {levelModel.LevelIndex}";
                Toolbox.LevelNumberLabel.gameObject.SetActive(true);
                Toolbox.CompletedLabel.gameObject.SetActive(true);
                _hero.SetState(WalkingHeroStateBuilder.Instance.GetVictoryState(_hero, Toolbox));
            }
        }
    }
    
}

public delegate void EnemyKilledOnPositionDelegate(EnemyBehaviour enemy, IEnemyPosition position);