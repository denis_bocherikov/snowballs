﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoAndAttackPositionCollectionBehaviour : BasePositionCollectionBehaviour, IHeroPosition
{
    public override IPlayerState GetPreActionState(ToolboxBehaviour toolbox, HeroBehaviour hero)
    {
        return WalkingHeroStateBuilder.Instance.GetGoToPositionAndAttackState(HeroAttackPosition,
            toolbox, hero, IsNeedShowRotateTip);
    }

    public override IPlayerState GetAfterActionState(ToolboxBehaviour toolbox, HeroBehaviour hero)
    {
        return null;
    }
}
