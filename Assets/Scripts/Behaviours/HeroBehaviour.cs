﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

public class HeroBehaviour : PlayerBehaviour, ITarget, IPlayer, IAttackingPlayer
{
	public HeroDamageCanvasBehaviour HeroDamageBehaviour;

	public float RotationSpeed = 0.03f;
	
	public GameObject CameraPosition;
	public Vector3 HandAttackPosition => SnowballPosition.transform.position;
	public bool IsCanBeAttacked => true;
	public bool IsCanAttack => true;
	public float MinimumAttackSpeed { get; private set; }
	public SimpleActionDelegate OnDie;
	public SimpleActionDelegate OnArriveToPosition;


	private ITarget _selectedTarget;
	private SnowballThrowBehaviour _throwBehaviour;
	private bool _isCurrentlyAttacking = false;
	private IPlayerState _currentState;
	private HeroAttackPositionBehaviour _currentAttackPosition;
	private bool _isLevelCompleted;
	private TargetsContainer _targets;
	private HeroProgress _progress;
	private float _health;
	private float _startHealth;
	public bool IsDefeated;
	private bool _isWin;

	private float _healthSliderCoefficient;

	public void Init(ToolboxBehaviour toolbox, GameDirectorBehaviour director, HeroProgress progress, TargetsContainer targets, float health)
	{
		_isWin = false;
		_targets = targets;
		IsAlive = true;		
		_toolbox = toolbox;
		_throwBehaviour = GetComponent<SnowballThrowBehaviour>();
		_throwBehaviour.Spline = _toolbox.ThrowSpline;
		HeroDamageBehaviour = director.HeroDamageBehaviour;
		Boy.AttackSpeed = progress.Speed;
		MinimumAttackSpeed = progress.Speed;
		_progress = progress;
		_health = health;
		_startHealth = health;
		_throwBehaviour.Rejection = progress.Rejection;
		_healthSliderCoefficient = 1 / health;
		
	}
	// Start is called before the first frame update
    void Start()
    {
        var collisionBehaviour = GetComponent<PlayerCollisionBehaviour>();
        collisionBehaviour.BoyModelBehaviour = BoyModelBehaviour;
    }

	// Update is called once per frame
    void Update()
    {
	    _currentState?.Update(this);
//	    if (_health < _progress.Health)
//	    {
//		    _health += Time.deltaTime;
//	    }

	    _toolbox.HealthSlider.value = _health * _healthSliderCoefficient;
    }

	private void LateUpdate()
	{
		_currentState?.LateUpdate(this);
	}

	public Vector3 TargetPosition => Boy.gameObject.transform.position;


	public override void Die()
	{
		throw new System.NotImplementedException();
	}

	public void Damage(float value)
	{

		if (iOSHapticFeedback.Instance.IsSupported())
		{
			iOSHapticFeedback.Instance.Trigger(iOSHapticFeedback.iOSFeedbackType.ImpactHeavy);
		}
		HeroDamageBehaviour.Damage(value);
		_health -= value;

		var fullPileHeight = _toolbox.MaxSnowPilePositionYOffset - _toolbox.MinSnowPilePositionYOffset;
		var pileOffset = fullPileHeight * (_health) / _startHealth;
		var pilePosition = _toolbox.SnowPile.transform.position;
		_toolbox.SnowPile.transform.position = new Vector3(pilePosition.x, transform.position.y - pileOffset - 0.14f, pilePosition.z);

		if (_health < 0f && !IsDefeated && !_isWin)
		{
			IsDefeated = true;
			OnDie.Invoke(this, EventArgs.Empty);
			SetState(WalkingHeroStateBuilder.Instance.GetDefeatState(this, _toolbox));
		}
	}

	public void Stun()
	{
		throw new NotImplementedException();
	}

	public void SetState(IPlayerState state)
	{
		_currentState?.Stop();
		_currentState = state;
		_currentState.Act(this);
	}

	public void Celebrate()
	{
		_isWin = true;
		_toolbox.SnowPile.SetActive(false);
		_isLevelCompleted = true;
		Boy.Victory();
	}

	private void OnDestroy()
	{
		_currentState?.Stop();
	}
}
