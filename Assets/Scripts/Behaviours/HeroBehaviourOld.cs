﻿//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Xml.Linq;
//using Battlehub.SplineEditor;
//using UnityEngine;
//using UnityEngine.AI;
//
//public class HeroBehaviour : MonoBehaviour, IPlayer
//{
//	private float _currentRotation = 0f;
//	private Vector3 _originalRotation;
//	private float RotationSpeed = 0.03f;
//	private Vector3 _startTouchPosition;
//	private float _maxRotationAngle = 45f;
//	private EnemyBehaviour _selectedEnemy;
//	private bool _isCanThrow = true;
//	private List<EnemyPosition> _enemies = new List<EnemyPosition>();
//	private bool _isCanRotate = true;
//	private List<SplineFollow> _throwedSnowballs = new List<SplineFollow>();
//	private bool _isVictory;
//	public List<EnemyBehaviour> Enemies;
//	public SplineFollow Snowball;
//	public bool IsShouldAvoid;
//	public bool IsUnderAttack;
//	public float SuccessThrowChance = 50;
//	public Camera Camera;
//	public DirectorBehaviour Director;
//	public GameObject SnowballStartPosition;
//	private BoyBehaviour _boy;
//
//	void Start ()
//	{
////		_boy = GetComponent<BoyBehaviour>();
//		_originalRotation = transform.rotation.eulerAngles;
//		_currentRotation = _originalRotation.y;
//		var c = GetComponent<NavMeshAgent>();
//		c.destination = new Vector3(-2.5f, -0.3f, 8.6f);
//		IsAlive = true;
//	}
//
//	void initDictionary()
//	{
//		foreach (var enemy in Enemies)
//		{
//			if (enemy != null)
//			{
//				_enemies.Add(new EnemyPosition{Enemy = enemy});
//			}
//		}
//	}
//	
//	void LateUpdate () {
//		ProcessTouch();
//		SelectEnemy();
//	}
//
//	#region Touch
//	void ProcessTouch()
//	{
//		if (_isVictory)
//		{
//			return;
//		}
//		if (Input.touchCount > 0 && !IsUnderAttack)
//		{
//			var touch = Input.touches[0];
//			if (touch.phase == TouchPhase.Moved)
//			{
//				ProcessMoveTouch(touch);
//			}
//			else if(touch.phase == TouchPhase.Began)
//			{
//				ProcessStartTouch(touch);
//			}
//			else if (touch.phase == TouchPhase.Ended)
//			{
//				ProcessEndTouch(touch);
//			}
////			else if(touch.phase == TouchPhase.Stationary)
////			{
////				_startTouchPosition = touch.position;
////				Debug.Log("touch stationary");
////			}
//		}
//	}
//
//	void ProcessStartTouch(Touch touch)
//	{
//		_startTouchPosition = touch.position;
//	}
//
//	void ProcessMoveTouch(Touch touch)
//	{
//		//process only rotation
//		var deltaX = _startTouchPosition.x - touch.position.x;
//		var deltaY = _startTouchPosition.y - touch.position.y;
//		if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
//		{
//			var coeff = deltaX> 0 ? -1f: 1f;
//			_currentRotation += coeff * Time.deltaTime * RotationSpeed;
//			
//			_currentRotation = Mathf.Clamp(_currentRotation, _originalRotation.y - _maxRotationAngle,
//				_originalRotation.y + _maxRotationAngle);
//			transform.eulerAngles = new Vector3(_originalRotation.x, _currentRotation, _originalRotation.z);
//		}
//	}
//
//	void ProcessEndTouch(Touch touch)
//	{
//		var deltaX = _startTouchPosition.x - touch.position.x;
//		var deltaY = _startTouchPosition.y - touch.position.y;
//		if (Mathf.Abs(deltaY) > Mathf.Abs(deltaX))
//		{
//			if (_isCanThrow && _selectedEnemy != null)
//			{
//				Debug.Log("end touch");
//				_boy.Attack();
//				_isCanThrow = false;
//				_isCanRotate = false;
//			}
//		}
//	}
//	#endregion
//
//	private void ThrowSnowball()
//	{
//		Debug.Log("start throw");
//		var newSnowball = Instantiate(Snowball.gameObject);
//		var newSnowballSplineFollow = newSnowball.GetComponent<SplineFollow>();
//		var spline = GetSpline();
//
//		var newSpline = Instantiate(spline);
//		
//		SetSpline(newSpline, SnowballStartPosition.transform.position, _selectedEnemy.transform.position);
//		
//		newSnowballSplineFollow.Completed.AddListener(OnThrowComplete);
//		newSnowballSplineFollow.gameObject.SetActive(true);
//		newSnowballSplineFollow.Spline = newSpline;
//		newSnowballSplineFollow.Speed = _selectedEnemy.SnowballSpeed;
//		newSnowballSplineFollow.enabled = true;
//		newSnowballSplineFollow.IsRunning = true;
//		
//		_throwedSnowballs.Add(newSnowballSplineFollow);
//	}
//
//	private void SetSpline(SplineBase spline, Vector3 fromPosition, Vector3 targetPosition)
//	{
//		var directionVector = targetPosition - fromPosition;
//		spline.SetControlPoint(0, fromPosition);
//		var lastCpp = targetPosition + directionVector.normalized * 3;
//		spline.SetControlPoint(3, lastCpp);		
//		
//		var firstCppInitialY = spline.GetControlPoint(1).y;
//		var secondCppInitialY = spline.GetControlPoint(2).y;
//
//		var resultDirectionVector = lastCpp - fromPosition;
//		
//		var firstCpp = fromPosition + resultDirectionVector / 4;
//		var secondCpp = fromPosition + resultDirectionVector / 4 * 3;
//		spline.SetControlPoint(1, new Vector3(firstCpp.x, firstCppInitialY, firstCpp.z));
//		spline.SetControlPoint(2, new Vector3(secondCpp.x, secondCppInitialY, secondCpp.z));
//
//	}
//
//	private SplineBase GetSpline()
//	{
//		SplineBase spline;
//		if (ThrowHelper.IsNextThrowSuccess(SuccessThrowChance))
//		{
//			spline = ThrowHelper.GetRandomSplineFromList(_selectedEnemy.HeroSuccessSplines);
//			_selectedEnemy.IsBlockedForMove = true;
//			_selectedEnemy.UnderAttack();
//		}
//		else
//		{
//			spline = ThrowHelper.GetRandomSplineFromList(_selectedEnemy.HeroFailSplines);
//		}
//
//		return spline;
//	}
//
//	private void OnThrowComplete()
//	{
//		var throwedSnowballs = new List<SplineFollow>(_throwedSnowballs);
//		foreach (var splineFollow in throwedSnowballs)
//		{
//			if (!splineFollow.IsRunning)
//			{
//				splineFollow.enabled = false;
//				splineFollow.gameObject.SetActive(false);
////				UnblockEnemy(splineFollow);
//				_throwedSnowballs.Remove(splineFollow);
//			}
//		}
//		if (Enemies.Count(a => a.IsActive) == 0)
//		{
//			Director.IsLevelEnd = true;
//			_isVictory = true;
//			_boy.Victory();
//			Camera.transform.SetParent(transform.parent);
//			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 180, transform.eulerAngles.z);
//		}
//	}
//
//	private void OnThrowMoveCompleted()
//	{
//		_isCanThrow = true;
//		_isCanRotate = true;
//		//_animator.Play("MakeSnowball");
//	}
//
//	protected void SnowballWasMade()
//	{
//		
//	}
//
//	private void SelectEnemy()
//	{
//		_selectedEnemy = GetSelectedEnemy();
//		foreach (var enemy in Enemies)
//		{
//			if (enemy != null)
//			{
//				enemy.Unselect();
//			}
//		}
//
//		if (_selectedEnemy != null && !_selectedEnemy.IsInShadow)
//		{
//			_selectedEnemy.Select();	
//		}
//	}
//
//	private EnemyBehaviour GetSelectedEnemy()
//	{
//		foreach (var enemy in Enemies)
//		{
//			if (enemy != null)
//			{
//				var angle = CalculateAngleForEnemy(enemy);
//				if (angle < 10)
//					return enemy;
//			}
//		}
//		
//		return null;
//	}
//
//	private float CalculateAngleForEnemy(EnemyBehaviour enemy)
//	{
//		var angle = Vector3.Angle(transform.forward, enemy.transform.position - transform.position);
//		return angle;
//	}
//
//
//
//	private void AvoidCompleted()
//	{
//		_isCanThrow = true;
//		_isCanRotate = true;
////		if (!_isCanThrow)
////		{
////			_animator.Play("MakeSnowball");
////		}
//	}
//
//	private void OnDestroy()
//	{
//		Director = null;
//	}
//
//	public bool IsAlive { get; set; }
//
//	public void Die()
//	{
//		_boy.Die();
//	}
//}
