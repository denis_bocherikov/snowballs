﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroDamageCanvasBehaviour : MonoBehaviour
{
    public RawImage SplashesImage;
    public void Damage(float value)
    {
        SplashesImage.gameObject.SetActive(true);
        var animator = GetComponent<Animator>();
        animator.Play("Damage");
    }

    public void Hide()
    {
        SplashesImage.gameObject.SetActive(false);
    }
}
