﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HeroProgressBehaviour : MonoBehaviour
{
    public Button SpeedButton;
    public Button RejectionButton;
    public Button OfflineEarnButton;

    public Text SpeedCostText;
    public Text RejectionCostText;
    public Text OfflineEarnCostText;

    public Text Money;

    private float[] _speedImproveSteps = {0.1f, 0.08f, 0.03f};
    private float[] _rejectionImproveSteps = {0.03f, 0.02f, 0.01f};
    private float[] _offlineEarnImproveSteps = {0.5f};

    private void Start()
    {
        UpdateMenuState();
    }

    public void UpdateMenuState()
    {
        var progressModel = GetCurrentProgress();
        var costModel = GetCurrentCostModel();
        UpdateButtonStates(costModel, progressModel);
    }

    public void ResetModels()
    {
        ResetProgressModel();
        ResetImproveModel();
        ResetCostModel();
        UpdateMenuState();
        LevelService.Instance.HardResetLevelModel();
    }

    private HeroProgress ResetProgressModel()
    {
        var model = new HeroProgress
        {
            CurrentLevel = 1,
            Rejection = 0.4f,
            Money = 0,
            Speed = 0.8f,
            OfflineEarn = 0
        };
        SaveCurrentProgress(model);
        return model;
    }

    private ImproveModel ResetImproveModel()
    {
        var model = new ImproveModel
        {
            RejectionLevel = 0,
            SpeedLevel = 0,
            OfflineEarnLevel = 0
        };
        SaveCurrentImproveModel(model);
        return model;
    }

    private CostModel ResetCostModel()
    {
        var model = new CostModel
        {
            Rejection = 10,
            Speed = 10,
            OfflineEarn = 20
        };
        SaveCurrentCostModel(model);
        return model;
    }

    public HeroProgress GetCurrentProgress()
    {
        var current = PlayerPrefs.GetString("progress");
        HeroProgress model;
        if (string.IsNullOrEmpty(current))
        {
            model = ResetProgressModel();
        }
        else
        {
            model = JsonUtility.FromJson<HeroProgress>(current);
        }

        return model;
    }

    private void SaveCurrentProgress(HeroProgress model)
    {
        var json = JsonUtility.ToJson(model);
        PlayerPrefs.SetString("progress", json);
        PlayerPrefs.Save();
    }
    
    public ImproveModel GetCurrentImproveModel()
    {
        var current = PlayerPrefs.GetString("improve");
        ImproveModel model;
        if (string.IsNullOrEmpty(current))
        {
            model = ResetImproveModel();
        }
        else
        {
            model = JsonUtility.FromJson<ImproveModel>(current);
        }

        return model;
    }

    private void SaveCurrentImproveModel(ImproveModel model)
    {
        var json = JsonUtility.ToJson(model);
        PlayerPrefs.SetString("improve", json);
        PlayerPrefs.Save();
    }
    
    public CostModel GetCurrentCostModel()
    {
        var current = PlayerPrefs.GetString("cost");
        CostModel model;
        if (string.IsNullOrEmpty(current))
        {
            model = ResetCostModel();
        }
        else
        {
            model = JsonUtility.FromJson<CostModel>(current);
        }

        return model;
    }

    private void SaveCurrentCostModel(CostModel model)
    {
        var json = JsonUtility.ToJson(model);
        PlayerPrefs.SetString("cost", json);
        PlayerPrefs.Save();
    }

    private float GetElement(float[] array, int index)
    {
        if (array.Length > index)
        {
            return array[index];
        }
        return array.Last();
    }

    private void UpdateButtonStates(CostModel newCostModel, HeroProgress newProgressModel)
    {
        SpeedButton.interactable = newProgressModel.Money >= newCostModel.Speed;
        RejectionButton.interactable = newProgressModel.Money >= newCostModel.Rejection;
        OfflineEarnButton.interactable = newProgressModel.Money >= newCostModel.OfflineEarn;
        
        SpeedCostText.text = newCostModel.Speed.ToString();
        RejectionCostText.text = newCostModel.Rejection.ToString();
        OfflineEarnCostText.text = newCostModel.OfflineEarn.ToString();

        Money.text = ((int)newProgressModel.Money).ToString();
    }

    public void AddSpeed()
    {
        var progressModel = GetCurrentProgress();
        var improveModel = GetCurrentImproveModel();
        var costModel = GetCurrentCostModel();
        if (progressModel.Money >= costModel.Speed)
        {
            progressModel.Money -= costModel.Speed;
            var improvement = GetElement(_speedImproveSteps, improveModel.SpeedLevel);
            progressModel.Speed += improvement;
            improveModel.SpeedLevel++;
            costModel.Speed += (int)(costModel.Speed * 0.3f);
            
            SaveCurrentProgress(progressModel);
            SaveCurrentImproveModel(improveModel);
            SaveCurrentCostModel(costModel);

            UpdateButtonStates(costModel, progressModel);
        }
    }
    public void AddAccuracy()
    {
        var progressModel = GetCurrentProgress();
        var improveModel = GetCurrentImproveModel();
        var costModel = GetCurrentCostModel();
        if (progressModel.Money >= costModel.Rejection)
        {
            progressModel.Money -= costModel.Rejection;
            var improvement = GetElement(_rejectionImproveSteps, improveModel.RejectionLevel);
            progressModel.Rejection -= improvement;
            improveModel.RejectionLevel++;
            costModel.Rejection += (int)(costModel.Rejection * 0.3f);
            
            SaveCurrentProgress(progressModel);
            SaveCurrentImproveModel(improveModel);
            SaveCurrentCostModel(costModel);
            
            UpdateButtonStates(costModel, progressModel);
        }
    }
    public void AddOfflineEarn()
    {
        var progressModel = GetCurrentProgress();
        var improveModel = GetCurrentImproveModel();
        var costModel = GetCurrentCostModel();
        if (progressModel.Money >= costModel.OfflineEarn)
        {
            progressModel.Money -= costModel.OfflineEarn;
            var improvement = GetElement(_offlineEarnImproveSteps, improveModel.OfflineEarnLevel);
            progressModel.OfflineEarn += improvement;
            improveModel.OfflineEarnLevel++;
            costModel.OfflineEarn += (int)(costModel.OfflineEarn * 0.3f);
            
            SaveCurrentProgress(progressModel);
            SaveCurrentImproveModel(improveModel);
            SaveCurrentCostModel(costModel);
            
            UpdateButtonStates(costModel, progressModel);
        }
    }

    public void AddPoint(float value)
    {
        var progressModel = GetCurrentProgress();
        progressModel.Money += value;
        SaveCurrentProgress(progressModel);
    }
}

[Serializable]
public class HeroProgress
{
    public int CurrentLevel;
    public float Money;
    public float OfflineEarn;
    public float Rejection;
    public float Speed;
    public float Health = 3;

    public HeroProgress()
    {
        CurrentLevel = 1;
    }
}

[Serializable]
public class ImproveModel
{
    public int SpeedLevel;
    public int RejectionLevel;
    public int OfflineEarnLevel;
}

[Serializable]
public class CostModel
{
    public float Speed;
    public float Rejection;
    public float OfflineEarn;
}