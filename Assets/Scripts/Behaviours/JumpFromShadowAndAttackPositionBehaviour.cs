﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpFromShadowAndAttackPositionBehaviour : BaseEnemyPositionBehaviour
{
    public GameObject AttackPosition;
    public GameObject ShadowPosition;
    public bool IsNeedGoToShadow = true;

    public override IPlayerState GetEnemyOnPositionState(BoyModelBehaviour modelBehaviour, ITarget target, IPlayer player, ToolboxBehaviour toolbox)
    {
        if (IsNeedGoToShadow)
        {
            return WalkingEnemyStateBuilder.Instance.GetJumpAttackAndGoToShadowLoopState(modelBehaviour.SnowballsCount,
                ShadowPosition.transform.position,
                AttackPosition.transform.position, target, player, toolbox);
        }
        else
        {
            return WalkingEnemyStateBuilder.Instance.GetJumpAttackAndIdleLoopState(modelBehaviour.SnowballsCount,
                AttackPosition.transform.position, target, player, toolbox);
        }
        
    }
}
