﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpRespawnBehaviour : BaseEnemyRespawnBehaviour
{
    private int _index;
    public List<JumpOverBehaviour> JumpPositions; 
    public override IPlayerState GetPlayerRespawnState(ToolboxBehaviour toolbox, IPlayer player, ITarget target)
    {
        var state = WalkingEnemyStateBuilder.Instance.GetJumpOverState(JumpPositions[_index], target);
        _index++;
        if (_index >= JumpPositions.Count)
        {
            _index = 0;
        }

        return state;
    }
}
