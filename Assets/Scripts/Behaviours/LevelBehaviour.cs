﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBehaviour : MonoBehaviour
{
    public List<BasePositionCollectionBehaviour> SubLevels;
    public bool IsShouldLoose;
    public LooseMoment LooseAt = LooseMoment.Middle;
}
