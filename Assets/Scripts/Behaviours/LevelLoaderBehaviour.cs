﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;

public class LevelLoaderBehaviour : MonoBehaviour
{
    private LocationBehaviour _currentLocation; 
    public List<LocationBehaviour> Levels;

    private void Awake()
    {
        GameAnalytics.Initialize();
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadLevel();
    }

    public void LoadLevel()
    {
        var locationService = LevelService.Instance;
        var levelModel = locationService.GetLevelModel();
        if (levelModel.LocationIndex >= Levels.Count)
        {
            levelModel = locationService.ResetLevelModel();
        }

        if (_currentLocation != null)
        {
            Destroy(_currentLocation.gameObject);
        }
        _currentLocation = Instantiate(Levels[levelModel.LocationIndex]);
        _currentLocation.GameDirector.LevelLoader = this;
    }

    public void LoadNextLevel()
    {
        var levelIndex = PlayerPrefs.GetInt("CurrentLevelIndex");
        levelIndex++;
        PlayerPrefs.SetInt("CurrentLevelIndex", levelIndex);
        LoadLevel();
    }
}
