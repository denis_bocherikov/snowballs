﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LocationBehaviour : MonoBehaviour
{
    public GameDirectorBehaviour GameDirector;
    
    public List<LevelBehaviour> Levels;
}
