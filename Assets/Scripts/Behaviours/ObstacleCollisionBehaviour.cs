﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollisionBehaviour : MonoBehaviour, ICollisionObject
{

	public GameObject DummySnowball;
	public void ProcessCollision(ISnowball snowball, Vector3 contactPoint)
	{
		snowball.DestroySnowball();
	}
}
