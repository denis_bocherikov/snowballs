﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public BoyBehaviour Boy;
    public BoyModelBehaviour BoyModelBehaviour { get; set; }
    public ITarget Target;
    public bool IsAlive { get; set; }
    public GameObject SnowballPosition;

    protected ToolboxBehaviour _toolbox;
    
    
    public ISnowball GetSnowball()
    {
        var result = Instantiate(_toolbox.SimpleSnowball);
        result.Owner = gameObject;
        return result;
    }

    public virtual void Die()
    {
        throw new NotImplementedException();
    }
}
