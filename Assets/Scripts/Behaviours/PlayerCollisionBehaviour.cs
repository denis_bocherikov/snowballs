﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerCollisionBehaviour : MonoBehaviour, ICollisionObject
{
	public BoyModelBehaviour BoyModelBehaviour;

	public void ProcessCollision(ISnowball snowball, Vector3 contactPoint)
	{
		var player = GetComponent<IPlayer>();

		if (gameObject.Equals(snowball.Owner))
		{
			return;
		}
		
		var newHealth = BoyModelBehaviour.Health - snowball.Power;
		if (player.IsCanBeAttacked)
		{
			player.Damage(snowball.Power);
			if (newHealth > 0)
			{
				BoyModelBehaviour.Health = newHealth;
				
				snowball.AffectPlayer(player);
			}
			else
			{
				player.Die();
			}
		}

		snowball.DestroySnowball();
	}
}
