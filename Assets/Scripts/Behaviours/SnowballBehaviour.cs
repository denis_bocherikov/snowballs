﻿using System.Collections;
using System.Collections.Generic;
using Battlehub.SplineEditor;
using UnityEngine;

public class SnowballBehaviour : MonoBehaviour, ISnowball
{
	public float DemagePower;
	public GameObject Dummy;
	public GameObject Owner { get; set; }

	public float Power { get; set; }

	public GameObject DummySnowball => Dummy;

	public Vector3 ViewerPosition { get; set; }

	// Use this for initialization
	void Start ()
	{
		Power = DemagePower;
		var splineFollow = GetComponent<SplineFollow>();
		splineFollow.Completed.AddListener(OnEndFlight);
	}
	
	public void DestroySnowball()
	{
		OnEndFlight();
	}
	
	public void AffectPlayer(IPlayer player)
	{
		
	}

	private void OnEndFlight()
	{
		var splineFollow = GetComponent<SplineFollow>();
		if (splineFollow != null)
		{
			Destroy(splineFollow.Spline.gameObject);
		}
		Destroy(gameObject);
	}

	private void OnCollisionEnter(Collision other)
	{
		var collisionObject = other.gameObject.GetComponent<ICollisionObject>();
		if (collisionObject != null)
		{
			collisionObject.ProcessCollision(this, other.contacts[0].point);
		}
	}
}
