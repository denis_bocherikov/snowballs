﻿using System;
using System.Collections;
using System.Collections.Generic;
using Battlehub.SplineEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class SnowballThrowBehaviour : MonoBehaviour
{
    public Vector3 HandPosition { get; set; }
    public Vector3 TargetPosition { get; set; }
    public SplineBase Spline { get; set; }
    public ISnowball Snowball { get; set; }
    public float Rejection = 0.1f;
    
	public void ThrowSnowball()
	{
		Snowball.gameObject.SetActive(true);
		Snowball.ViewerPosition = gameObject.transform.position;
		var newSnowballSplineFollow = Snowball.gameObject.GetComponent<SplineFollow>();

		var newSpline = Instantiate(Spline);
		Random.InitState(DateTime.Now.Millisecond);
		var targetPosition =
			TargetPosition + new Vector3(Random.Range(-Rejection, Rejection), Random.Range(-Rejection, Rejection),
				Random.Range(-Rejection, Rejection));
		
		SetSpline(newSpline, HandPosition, targetPosition);
		
		newSnowballSplineFollow.gameObject.SetActive(true);
		newSnowballSplineFollow.Spline = newSpline;
		newSnowballSplineFollow.Speed = 7;
		newSnowballSplineFollow.enabled = true;
		newSnowballSplineFollow.IsRunning = true;
	}

	private void SetSpline(SplineBase spline, Vector3 fromPosition, Vector3 targetPosition)
	{
		var directionVector = targetPosition - fromPosition;
		spline.SetControlPoint(0, fromPosition);
		var lastCpp = targetPosition + directionVector.normalized * 3;
		spline.SetControlPoint(3, lastCpp);		
		
		var firstCppInitialY = spline.GetControlPoint(1).y;
		var secondCppInitialY = spline.GetControlPoint(2).y;

		var resultDirectionVector = lastCpp - fromPosition;
		
		var firstCpp = fromPosition + resultDirectionVector / 4;
		var secondCpp = fromPosition + resultDirectionVector / 4 * 3;
		spline.SetControlPoint(1, new Vector3(firstCpp.x, firstCppInitialY, firstCpp.z));
		spline.SetControlPoint(2, new Vector3(secondCpp.x, secondCppInitialY, secondCpp.z));

	}
}
