﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayEnemyPositionBehaviour : BaseEnemyPositionBehaviour
{
    public override IPlayerState GetEnemyOnPositionState(BoyModelBehaviour modelBehaviour, ITarget target, IPlayer player, ToolboxBehaviour toolbox)
    {
        return WalkingEnemyStateBuilder.Instance.GetStayEnemyPosition(target, player, modelBehaviour, toolbox);
    }
}
