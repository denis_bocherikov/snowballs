﻿using System.Collections;
using System.Collections.Generic;
using Battlehub.SplineEditor;
using UnityEngine;

public class StunSnowballBehaviour : MonoBehaviour, ISnowball
{
    public float DemagePower;
    public float Power { get; set; }
    public GameObject DummySnowball { get; }
    public Vector3 ViewerPosition { get; set; }
    
    void Start ()
    {
        Power = DemagePower;
        var splineFollow = GetComponent<SplineFollow>();
        splineFollow.Completed.AddListener(OnEndFlight);
    }
    public void DestroySnowball()
    {
        OnEndFlight();
    }
    
    private void OnEndFlight()
    {
        var splineFollow = GetComponent<SplineFollow>();
        if (splineFollow != null && splineFollow.Spline != null && splineFollow.Spline.gameObject != null)
        {
            Destroy(splineFollow.Spline.gameObject);
        }
        Destroy(gameObject);
    }

    public GameObject Owner { get; }
    public void AffectPlayer(IPlayer player)
    {
        player.Stun();
    }
    
    private void OnCollisionEnter(Collision other)
    {
        var collisionObject = other.gameObject.GetComponent<ICollisionObject>();
        if (collisionObject != null)
        {
            collisionObject.ProcessCollision(this, other.contacts[0].point);
        }
    }
}
