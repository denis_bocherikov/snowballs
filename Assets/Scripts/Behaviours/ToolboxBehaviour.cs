﻿using System;
using System.Collections;
using System.Collections.Generic;
using Battlehub.SplineEditor;
using GameAnalyticsSDK.Setup;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public delegate void VoidFunc();
public class ToolboxBehaviour : MonoBehaviour
{
    public SnowballBehaviour SimpleSnowball;
    public StunSnowballBehaviour StunSnowball;
    public SplineBase ThrowSpline;
    public GameObject TargetRing;
    public TargetsContainer TargetsContainer;
    public Camera Camera;
    public Vector3 CameraOffsetPosition;
    public Vector3 VictoryCameraOffsetPosition;
    public Text RotateTipLabel;
    public Slider RotateTipSlider;
    public Text LevelNumberLabel;
    public Text CompletedLabel;
    public ParticleSystem Confetti;
    public Vector3 ConfettiOffset;
    public Slider SpeedSlider;
    public Slider DevilSlider;
    public EnemyThrowAccuracyModelBehavior EnemyAccuracy;
    public RawImage Fail;
    public List<RawImage> NiceWords;
    public Text TapTip;
    public Text TouchToPlayTip;
    public EnemyDamagePointBehavior Point;
    public GameObject SplashesCanvas;
    public Slider HealthSlider;
    public GameObject HealthIcon;
    public GameObject SpeedIcon;
    public GameObject SnowPile;
    public GameObject SnowPilePrefab;
    public float MaxSnowPilePositionYOffset = -0.19f;
    public float MinSnowPilePositionYOffset = -0.51f;

    public void Start()
    {
        SnowPile = Instantiate(SnowPilePrefab);
    }

    public void OnDestroy()
    {
        Destroy(SnowPile);
    }
}
