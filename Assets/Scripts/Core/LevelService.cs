﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class LevelService
{
    private const string _key = "locationAndLevelModel";
    private const string _additionalEnemyModelKey = "additionalEnemyModel";

    private LevelService()
    {
    }

    public static LevelService Instance { get; } = new LevelService();

    public LevelModel GetLevelModel()
    {
        var modelString = PlayerPrefs.GetString(_key);
        if (!string.IsNullOrEmpty(modelString))
        {
            return JsonUtility.FromJson<LevelModel>(modelString);
        }

        var model = new LevelModel();// {LocationIndex = 6, LocationSubLevel = 0};
        
        SaveModel(model);
        return model;
    }

    public LevelModel CompleteLocation()
    {
        var model = GetLevelModel();
        model.LocationSubLevel = 0;
        model.LocationIndex++;
        SaveModel(model);
        return model;
    }

    public LevelModel CompleteLocationSubLevel()
    {
        var model = GetLevelModel();
        model.LevelIndex++;
        model.LocationSubLevel++;
        SaveModel(model);
        return model;
    }

    public LevelModel ResetLevelModel()
    {
        IncreaseAdditionalEnemyModel();
        var model = GetLevelModel();
        var newModel = new LevelModel { LocationIndex = 1};
        newModel.LevelIndex = model.LevelIndex;
        SaveModel(newModel);
        return newModel;
    }

    public void HardResetLevelModel()
    {
        var model = new LevelModel();
        SaveModel(model);
    }

    public int GetAdditionalEnemyModelCoefficient()
    {
        return PlayerPrefs.GetInt(_additionalEnemyModelKey, 1);
    }

    private void SaveModel(LevelModel model)
    {
        var modelString = JsonUtility.ToJson(model);
        PlayerPrefs.SetString(_key, modelString);
    }

    private void IncreaseAdditionalEnemyModel()
    {
        var model = PlayerPrefs.GetInt(_additionalEnemyModelKey, 1);
        model++;
        PlayerPrefs.SetInt(_additionalEnemyModelKey, model);
    }
    
}

[Serializable]
public class LevelModel
{
    public int LevelIndex;
    public int LocationIndex;
    public int LocationSubLevel;
}

[Serializable]
public class AdditionalEnemyModel
{
    public float Health;
}