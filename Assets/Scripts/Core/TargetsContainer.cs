﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TargetsContainer
{
    private ConcurrentDictionary<ITarget, float> Items { get; set; } = new ConcurrentDictionary<ITarget, float>();

    public void Add(ITarget target)
    {
        while (!Items.TryAdd(target, 999))
        {
            
        }
    }

    public void Remove(ITarget target)
    {
        float temp;
        while (!Items.TryRemove(target, out temp))
        {
            
        }
    }

    public ICollection<ITarget> GetItems()
    {
        return Items.Keys;
    }
}
