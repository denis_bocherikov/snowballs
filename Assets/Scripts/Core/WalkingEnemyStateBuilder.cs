﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class WalkingEnemyStateBuilder
{

	private static WalkingEnemyStateBuilder _instance = new WalkingEnemyStateBuilder();

	private WalkingEnemyStateBuilder()
	{
		
	}

	public static WalkingEnemyStateBuilder Instance => _instance;

	public IPlayerState GetGoToPositionState(Vector3 destination, ITarget target)
	{
		return new GoToPositionState(destination, target);
	}

	public IPlayerState GetAttackState(int count, ITarget target, IAttackingPlayer player, ToolboxBehaviour toolbox)
	{
		return new AttackState(count, target, player, toolbox);
	}

	public IPlayerState GetJumpState(Vector3 position, bool isAbleToAttack, bool isAbleToBeAttackedDuringAllJump)
	{
		return new JumpState(position, isAbleToAttack, isAbleToBeAttackedDuringAllJump);
	}

	public StateSequence GetSequenceState()
	{
		return new StateSequence();
	}

	public IPlayerState GetIdleState(float? duration = null)
	{
		return new IdleState(duration);
	}

	public DamageState GetDamageState()
	{
		return new DamageState();
	}

	public IPlayerState GetInShadowState()
	{
		return new InShadowState();
	}

	public IPlayerState GetLookAtState(ITarget target)
	{
		return new LookAtTargetState(target.transform.position);
	}

	public IPlayerState GetWaitStartAttackState(IPlayer player, ITarget target)
	{
		return new WaitStartAttackState(player, target);
	}

	public IPlayerState GetJumpOverState(JumpOverBehaviour jumpPositions, ITarget target)
	{
		var sequence = GetSequenceState();
		sequence.SetNextState(GetLookAtState(target));
		sequence.SetNextState(GetJumpState(jumpPositions.UnderPosition.transform.position, false,false));
		sequence.SetNextState(GetJumpState(jumpPositions.EndPosition.transform.position, false,false));
		return sequence;
	}

	public IPlayerState GetJumpAttackAndGoToShadowLoopState(int attackSnowballsCount, Vector3 shadowPosition,
		Vector3 attackPosition, ITarget target, IPlayer player, ToolboxBehaviour toolbox)
	{
		var sequence = GetSequenceState();
		sequence.SetNextState(GetJumpState(attackPosition, false, true));
		sequence.SetNextState(GetWaitStartAttackState(player, target));
		sequence.SetNextState(GetAttackState(attackSnowballsCount, target, player, toolbox));
		sequence.SetNextState(GetIdleState());
		sequence.SetNextState(GetJumpState(shadowPosition, true, true));
		sequence.SetNextState(GetInShadowState());
		sequence.IsLooped = true;
		return sequence;
	}

	public IPlayerState GetJumpAttackAndIdleLoopState(int attackSnowballsCount,
		Vector3 attackPosition, ITarget target, IPlayer player, ToolboxBehaviour toolbox)
	{
		var sequence = GetSequenceState();
		var subSequence = GetSequenceState();
		
		sequence.SetNextState(GetJumpState(attackPosition, false, true));
		
		subSequence.SetNextState(GetWaitStartAttackState(player, target));
		subSequence.SetNextState(GetAttackState(attackSnowballsCount, target, player, toolbox));
		subSequence.SetNextState(GetIdleState());
		
		subSequence.IsLooped = true;
		sequence.SetNextState(subSequence);
		return sequence;
	}

	public IPlayerState GetStayEnemyPosition(ITarget target, IPlayer player, BoyModelBehaviour modelBehaviour, ToolboxBehaviour toolbox)
	{
		var sequence = GetSequenceState();
		
		sequence.SetNextState(GetWaitStartAttackState(player, target));
		sequence.SetNextState(GetAttackState(modelBehaviour.SnowballsCount, target, player, toolbox));
		sequence.SetNextState(GetIdleState());
		sequence.IsLooped = true;
		return sequence;
	}
}
