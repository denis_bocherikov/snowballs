﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingHeroStateBuilder
{
    private static WalkingHeroStateBuilder _instance = new WalkingHeroStateBuilder();
    private WalkingHeroStateBuilder()
    {
        
    }
    public static WalkingHeroStateBuilder Instance => _instance;

    public IPlayerState GetGoToPositionState(HeroAttackPositionBehaviour heroAttackPosition, ToolboxBehaviour toolbox, HeroBehaviour hero)
    {
        return new HeroGoToPositionState(heroAttackPosition, null, toolbox, hero);
    }

    public IPlayerState GetAttackState(ToolboxBehaviour toolbox, HeroBehaviour hero, bool isNeedShowRotateTip)
    {
        return new HeroAttackState(toolbox, hero, isNeedShowRotateTip);
    }

    public IPlayerState GetLookAtTargetState(Vector3 position)
    {
        return new LookAtTargetState(position);
    }

    public IPlayerState GetVictoryState(HeroBehaviour hero, ToolboxBehaviour toolbox)
    {
        return new HeroVictoryState(hero, toolbox);
    }
    
    public IPlayerState GetDefeatState(HeroBehaviour hero, ToolboxBehaviour toolbox)
    {
        return new HeroDefeatState(hero, toolbox);
    }

    public IPlayerState GetGoToPositionAndAttackState(HeroAttackPositionBehaviour attackPosition, ToolboxBehaviour toolbox, HeroBehaviour hero, bool isNeedShowRotateTip)
    {
        var result = new StateSequence();
        result.SetNextState(GetGoToPositionState(attackPosition, toolbox, hero));
        result.SetNextState(GetLookAtTargetState(attackPosition.LookAtPosition.transform.position));
        result.SetNextState(GetAttackState(toolbox, hero, isNeedShowRotateTip));
        return result;
    }
}
