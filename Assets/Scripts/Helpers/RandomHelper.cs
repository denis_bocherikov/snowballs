﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class RandomHelper<T>
{
    public static T GetRandomValue(IEnumerable<T> list)
    {
        var items = list.Where(a => a != null);
        var randomValue = Random.Range(0, list.Count());
        return list.ElementAt(randomValue);
    }
}
