﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Battlehub.SplineEditor;
using UnityEngine;

public static class ThrowHelper {

    public static SplineBase GetRandomSplineFromList(List<SplineBase> splines)
    {
        return splines.ElementAt(Random.Range(0, splines.Count));
    }

    public static bool IsNextThrowSuccess(float successChance)
    {
        var value = Random.Range(0, 101);
        return value < successChance;
    }
}
