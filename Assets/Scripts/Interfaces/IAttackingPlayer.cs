using UnityEngine;

public interface IAttackingPlayer
{
    Vector3 HandAttackPosition { get; }
}