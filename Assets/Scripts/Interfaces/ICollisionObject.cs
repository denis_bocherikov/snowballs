﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollisionObject
{
    void ProcessCollision(ISnowball snowball, Vector3 contactPoint);
}
