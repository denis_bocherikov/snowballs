using UnityEngine;

public interface IEnemyPosition
{ 
    Transform transform { get; }
    IPlayerState GetEnemyOnPositionState(BoyModelBehaviour modelBehaviour, ITarget target, IPlayer player, ToolboxBehaviour toolbox);
}