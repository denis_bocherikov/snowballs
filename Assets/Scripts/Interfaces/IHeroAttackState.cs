﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHeroAttackState
{
    GameObject Snowball { get; }
    SimpleActionDelegate OnStateCompleted { get; set; }
    void Tap();
    void Start();
    void Update();
    void Stun();
}
