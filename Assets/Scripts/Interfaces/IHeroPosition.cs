﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHeroPosition
{
    IPlayerState GetPreActionState(ToolboxBehaviour toolbox, HeroBehaviour hero);
    IPlayerState GetAfterActionState(ToolboxBehaviour toolbox, HeroBehaviour hero);
}
