﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer : IAttackingPlayer
{
    bool IsAlive { get; set; }
    bool IsCanBeAttacked { get; }
    bool IsCanAttack { get; }
    void Die();
    void Damage(float value);

    void Stun();
}
