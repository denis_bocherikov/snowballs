﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRespawn
{
    Transform transform { get; }
    IPlayerState GetPlayerRespawnState(ToolboxBehaviour toolbox, IPlayer player, ITarget target);
}
