﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISnowball
{
    GameObject gameObject { get; }
    float Power { get; set; }
    GameObject DummySnowball { get; }
    Vector3 ViewerPosition { get; set; }
    void DestroySnowball();
    GameObject Owner { get; }
    void AffectPlayer(IPlayer player);
}
