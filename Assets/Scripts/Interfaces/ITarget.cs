﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITarget
{
    Vector3 TargetPosition { get; }
    Transform transform { get; }
    bool IsCanBeAttacked { get; }
}
