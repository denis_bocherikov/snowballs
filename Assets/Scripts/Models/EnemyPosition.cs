﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPosition
{
    public EnemyBehaviour Enemy { get; set; }
    public float Angle { get; set; }
}
