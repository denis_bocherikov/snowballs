﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : BaseWalkingPlayerState {
	public override bool IsCanBeAttacked
	{
		get { return true; }
	}

	private readonly int _count;
	private readonly ITarget _target;
	private readonly IAttackingPlayer _player;
	private readonly ToolboxBehaviour _toolbox;
	private readonly ISnowball _snowball;
	private int _remainCount;
	private PlayerBehaviour _owner;
	private SnowballThrowBehaviour _throwBehaviour;
	
	public override bool IsCanBePaused => true;

	public AttackState(int count, ITarget target, IAttackingPlayer player, ToolboxBehaviour toolbox)
	{
		_count = count;
		_target = target;
		_player = player;
		_toolbox = toolbox;
	}

	public override void Act(PlayerBehaviour player)
	{
		_remainCount = _count;
		_owner = player;
		_throwBehaviour = _owner.GetComponent<SnowballThrowBehaviour>();
		_throwBehaviour.Spline = _toolbox.ThrowSpline;
		player.Boy.OnThrowEndDelegate += ThrowComplete;
		player.Boy.OnThrowStartDelegate += ThrowStart;
		player.Boy.Attack();
	}

	public override void Update(PlayerBehaviour player)
	{
		var targetRotation =
			Quaternion.LookRotation(_target.transform.position - player.transform.position, Vector3.up);
		player.transform.rotation =
			Quaternion.RotateTowards(player.transform.rotation, targetRotation, Time.deltaTime * 100f);
	}

	public override void Resume()
	{
		base.Resume();
		if (_remainCount > 0 && _owner != null)
		{
			_owner.Boy.Attack();
		}
	}

	protected override void ClearState()
	{
		if (_owner != null)
		{
			_owner.Boy.OnThrowEndDelegate -= ThrowComplete;
			_owner.Boy.OnThrowStartDelegate -= ThrowStart;
			_owner = null;
		}
	}

	private void ThrowComplete(object sender, EventArgs e)
	{
		_remainCount--;
		if (_owner != null && _owner.IsAlive && _remainCount <= 0 && !IsPaused)
		{
			Complete();		
		}
	}
	
	private void ThrowStart(object sender, EventArgs e)
	{
		if (!IsPaused)
		{
			_throwBehaviour.HandPosition = _owner.SnowballPosition.transform.position;
			_throwBehaviour.TargetPosition = _target.TargetPosition;
			_throwBehaviour.Snowball = _owner.GetSnowball();
			_throwBehaviour.Rejection = _toolbox.EnemyAccuracy.GetRejection();
			_throwBehaviour.ThrowSnowball();
		}
	}
}
