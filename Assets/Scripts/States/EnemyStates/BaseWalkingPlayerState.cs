﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

public abstract class BaseWalkingPlayerState : Object, IPlayerState
{

    public event SimpleActionDelegate OnStateError;
    protected bool _isStoped = false;
    public event SimpleActionDelegate StateCompleted;
    public virtual bool IsCompleted { get; private set; }
    protected bool IsPaused { get; set; }
    public abstract bool IsCanBePaused { get; }

    public virtual bool IsCanBeAttacked => false;

    protected abstract void ClearState();
    public abstract void Act(PlayerBehaviour player);
    public abstract void Update(PlayerBehaviour player);

    public virtual void LateUpdate(PlayerBehaviour player)
    {
        
    }
    
    public virtual void Stop()
    {
        _isStoped = true;
        StateCompleted = null;
        ClearState();
    }

    public virtual void Pause()
    {
        IsPaused = true;
    }

    public virtual void Resume()
    {
        IsPaused = false;
    }

    public virtual void Stun()
    {
        
    }

    protected virtual void Complete()
    {
        IsCompleted = true;
        ClearState();
        if (!_isStoped && StateCompleted != null)
        {
            StateCompleted.Invoke(this, null);
        }

        StateCompleted = null;
    }

    protected virtual void StateError()
    {
        if (OnStateError != null)
        {
            OnStateError.Invoke(this, EventArgs.Empty);
        }
    }
}
