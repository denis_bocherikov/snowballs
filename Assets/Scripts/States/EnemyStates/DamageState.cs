﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class DamageState : BaseWalkingPlayerState
{
    private readonly float Delay;
    private float _delay;

    public override bool IsCanBePaused => false;

    public DamageState()
    {
        Random.InitState(DateTime.Now.Millisecond);
        Delay = 3.5f;
    }
    
    
    public override void Act(PlayerBehaviour player)
    {
        _delay = Delay;
        if (player != null)
        {
            player.Boy.Idle();
            player.Boy.Stars.SetActive(true);
        }
    }

    public override void Update(PlayerBehaviour player)
    {
        _delay -= Time.deltaTime;
        player.Boy.Stars.transform.Rotate(0, Time.deltaTime * 20, 0);
        if (_delay < 0)
        {
            player.Boy.Stars.SetActive(false);
            Complete();
        }
    }

    public override void Stun()
    {
        _delay = Delay;
    }

    protected override void ClearState()
    {        
    }
}
