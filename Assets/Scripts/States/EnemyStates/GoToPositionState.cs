﻿using System;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.AI;

public class GoToPositionState : BaseWalkingPlayerState
{
    private readonly Vector3 Position;
    private float _distanceToPosition;
    private ITarget Target;
    private float _travelTime = 15f;
    
    public override bool IsCanBePaused => true;

    public GoToPositionState(Vector3 position, ITarget target)
    {
        Target = target;
        Position = position;
    }
    public override void Act(PlayerBehaviour player)
    {
        var navMeshAgent = player.GetComponent<NavMeshAgent>();
        navMeshAgent.enabled = true;
        player.Boy.Run();
    }

    public override void Update(PlayerBehaviour player)
    {
        if (!IsCompleted)
        {
            _travelTime -= Time.deltaTime;
            if (_travelTime < 0)
            {
                GameAnalytics.NewErrorEvent (GAErrorSeverity.Warning, "enemy didn't reach position");
                StateError();
            }
            var navMeshAgent = player.GetComponent<NavMeshAgent>();
            navMeshAgent.SetDestination(Position);

            _distanceToPosition =
                Vector2.Distance(new Vector2(player.transform.position.x, player.transform.position.z),
                    new Vector2(Position.x, Position.z));
            
            if (_distanceToPosition < 0.5f)
            {
                navMeshAgent.enabled = false;
                if (Target != null)
                {
                    player.transform.LookAt(Target.transform.position);    
                }
                player.Boy.Idle();
                Complete();
            }
        }
    }

    protected override void ClearState()
    {
        
    }
}
