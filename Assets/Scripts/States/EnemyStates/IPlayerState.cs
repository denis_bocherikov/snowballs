﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerState
{
    event SimpleActionDelegate StateCompleted;
    event SimpleActionDelegate OnStateError;
    bool IsCompleted { get; }
    bool IsCanBePaused { get; }
    bool IsCanBeAttacked { get; }
    void Act(PlayerBehaviour player);
    void Update(PlayerBehaviour player);
    void LateUpdate(PlayerBehaviour player);
    void Stop();
    void Pause();
    void Resume();
    void Stun();
}
