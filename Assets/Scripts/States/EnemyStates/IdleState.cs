using System;
using System.Diagnostics;
using Random = UnityEngine.Random;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class IdleState : BaseWalkingPlayerState
{
    private float Delay;
    private float _delay;
    
    public override bool IsCanBePaused
    {
        get { return true; }
    }

    public override bool IsCanBeAttacked
    {
        get { return true; }
    }

    public IdleState()
    {
        SetRandomDelay();
    }

    public IdleState(float? delay)
    {
        if (delay != null)
        {
            Delay = delay.Value;
        }
        else
        {
            SetRandomDelay();
        }
    }

    private void SetRandomDelay()
    {
        Random.InitState(DateTime.Now.Millisecond);
        Delay = Random.Range(1f, 3f);
    }
    public override void Act(PlayerBehaviour player)
    {
        _delay = Delay;
        player.Boy.Idle();
    }

    public override void Update(PlayerBehaviour player)
    {
        _delay -= Time.deltaTime;
        if (_delay < 0)
        {
            Complete();
        }
    }

    protected override void ClearState()
    {
        
    }
}