﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InShadowState : IdleState {
    public override bool IsCanBeAttacked
    {
        get { return false; }
    }
}
