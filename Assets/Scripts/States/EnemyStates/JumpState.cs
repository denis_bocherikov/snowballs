using System;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class JumpState : BaseWalkingPlayerState
{
    private PlayerBehaviour _enemy;
    private float _moveSpeed;
    private readonly Vector3 _position;
    private bool _isAbleToAttack;
    private readonly bool _isAbleToBeAttackedDuringAllJump;

    public override bool IsCanBePaused => false;

    public override bool IsCanBeAttacked => _isAbleToAttack && _isAbleToBeAttackedDuringAllJump;

    public JumpState(Vector3 position, bool isAbleToAttack, bool isAbleToBeAttackedDuringAllJump)
    {
        _position = position;
        _isAbleToAttack = isAbleToAttack;
        _isAbleToBeAttackedDuringAllJump = isAbleToBeAttackedDuringAllJump;
    }
    public override void Act(PlayerBehaviour player)
    {
        var navMeshAgent = player.GetComponent<NavMeshAgent>();
        navMeshAgent.enabled = false;
        _enemy = player;
        _enemy.Boy.OnJumpComplete += OnJumpComplete;
        _enemy.Boy.OnHalfJumpDelegate += OnHalfJumpComplete;
        _enemy.Boy.Jump();
        var boyAnimator = player.Boy.GetComponent<Animator>();
        var animations = boyAnimator.runtimeAnimatorController.animationClips;
        var jumpAnimationLength = animations.FirstOrDefault(a => a.name == "Jump").length;
        _moveSpeed = Vector3.Distance(player.transform.position, _position) / jumpAnimationLength * 1.8f;
    }

    public override void Update(PlayerBehaviour player)
    {
        if (_enemy != null)
        {
            _enemy.transform.position =
                Vector3.MoveTowards(_enemy.transform.position, _position, Time.deltaTime * _moveSpeed);
        }
    }

    private void OnJumpComplete(object sender, EventArgs e)
    {
        Complete();
        _isAbleToAttack = !_isAbleToAttack;
    }

    private void OnHalfJumpComplete(object sender, EventArgs e)
    {
        if (_isAbleToBeAttackedDuringAllJump)
        {
            _isAbleToAttack = !_isAbleToAttack;
        }
    }

    protected override void ClearState()
    {
        _enemy.Boy.OnJumpComplete -= OnJumpComplete;
        _enemy.Boy.OnHalfJumpDelegate -= OnHalfJumpComplete;
        _enemy = null;
    }
}