using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class StateSequence : BaseWalkingPlayerState
{
    public bool IsLooped { get; set; }

    public override bool IsCanBeAttacked => _currentState.Value.IsCanBeAttacked;

    private LinkedList<IPlayerState> _states = new LinkedList<IPlayerState>();
    private LinkedListNode<IPlayerState> _currentState;
    private IPlayerState _currentExecutionState;
    private PlayerBehaviour _player;
    private DamageState _damageState;
    
    public override bool IsCanBePaused => _currentExecutionState.IsCanBePaused;

    public override void Act(PlayerBehaviour player)
    {
        _player = player;
        _currentState = _states.First;
        _currentState.Value.StateCompleted += OnCurrentStateComplete;
        _currentState.Value.Act(player);
        _currentExecutionState = _currentState.Value;
    }

    public override void Update(PlayerBehaviour player)
    {
        _currentExecutionState?.Update(player);
    }

    public override void LateUpdate(PlayerBehaviour player)
    {
        _currentExecutionState?.LateUpdate(player);
    }

    protected override void ClearState()
    {
        _currentState.Value.StateCompleted -= OnCurrentStateComplete;
        _player = null;
    }

    public void SetNextState(IPlayerState state)
    {
        state.OnStateError += OnSubStateError;
        _states.AddLast(state);
    }

    public override void Stop()
    {
        _currentState.Value.StateCompleted -= OnCurrentStateComplete;
        _currentState.Value.Stop();
        base.Stop();
    }

    public override void Pause()
    {
        base.Pause();
        _currentState.Value.Pause();
    }

    public override void Resume()
    {
        base.Resume();
        _currentState.Value.Resume();
    }

    public override void Stun()
    {
        if (_isStoped)
        {
            return;
        }
        
        if (_damageState == null)
        {
            _damageState = WalkingEnemyStateBuilder.Instance.GetDamageState();
            _damageState.StateCompleted += OnDamageStateComplete;
            if (_currentState.Value.IsCanBePaused)
            {
                _currentState.Value.Pause();
                _currentExecutionState = _damageState;
                _damageState.Act(_player);
            }
            else
            {
                _states.AddAfter(_currentState, _damageState);
            }
        }
        else
        {
            _damageState.Stun();
        }
    }

    private void ExecuteNextState()
    {
        _currentState = _currentState.Next;
        _currentState.Value.StateCompleted += OnCurrentStateComplete;
        _currentState.Value.Act(_player);
        _currentExecutionState = _currentState.Value;
    }

    private void OnCurrentStateComplete(object sender, EventArgs e)
    {
        _currentState.Value.StateCompleted -= OnCurrentStateComplete;
        if (_currentState.Next == null)
        {
            if (IsLooped)
            {
                Act(_player);
            }
            else
            {
                Complete();
            }
        }
        else
        {
            ExecuteNextState();
        }
    }

    private void OnDamageStateComplete(object sender, EventArgs e)
    {
        _damageState.StateCompleted -= OnDamageStateComplete;
        
        if (_currentState.Value.IsCanBePaused)
        {
            _currentExecutionState = _currentState.Value;
            _currentExecutionState.Resume();
        }
        else
        {
            
            _states.Remove(_damageState);
            ExecuteNextState();
            
        }
        _damageState = null;
    }

    private void OnSubStateError(object sender, EventArgs e)
    {
        foreach (var state in _states)
        {
            state.OnStateError -= OnSubStateError;
        }
        StateError();
    }
}