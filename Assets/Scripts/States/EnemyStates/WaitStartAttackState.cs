﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitStartAttackState : BaseWalkingPlayerState
{
    private readonly IPlayer _player;
    private readonly ITarget _target;
    private bool _isCanAttack = false;
    private float _delay;
    public override bool IsCanBePaused => false;

    public WaitStartAttackState(IPlayer player, ITarget target)
    {
        _player = player;
        _target = target;
    }

    protected override void ClearState()
    {
        
    }

    public override void Act(PlayerBehaviour player)
    {
        if (_player.IsCanAttack)
        {
            Complete();
        }
        else
        {
            _delay = Random.Range(0, 1.5f);
        }
    }

    public override void Update(PlayerBehaviour player)
    {
        player.gameObject.transform.LookAt(_target.transform);
        if (_player.IsCanAttack)
        {
            _delay -= Time.deltaTime;
        }
        if (_delay <= 0)
        {
            Complete();
        }
    }
}
