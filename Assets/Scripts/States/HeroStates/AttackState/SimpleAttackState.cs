﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAttackState : IHeroAttackState
{
    private readonly HeroBehaviour _hero;
    private readonly ToolboxBehaviour _toolbox;
    private readonly float _minimumAttackSpeed;
    
    private const float _maximumSpeedUp = 0.7f;
    private readonly float _maximumSpeed;
    private readonly float _sliderCoefficient;
    private const float _speedUpTapBoost = 0.15f;
    private const float _speedReduceCoefficient = 0.3f;

    public SimpleAttackState(HeroBehaviour hero, ToolboxBehaviour toolbox, float minimumAttackSpeed)
    {
        _hero = hero;
        _toolbox = toolbox;
        _minimumAttackSpeed = minimumAttackSpeed;
        _maximumSpeed = _hero.MinimumAttackSpeed + _hero.MinimumAttackSpeed * _maximumSpeedUp;
        _sliderCoefficient = 1 / (_maximumSpeed - _hero.MinimumAttackSpeed);
    }


    public GameObject Snowball => _toolbox.SimpleSnowball.gameObject;
    public SimpleActionDelegate OnStateCompleted { get; set; }
    public void Tap()
    {
        if (_hero.Boy.AttackSpeed < _maximumSpeed)
        {
            _hero.Boy.AttackSpeed += _speedUpTapBoost;
        }

        if (_hero.Boy.AttackSpeed >= _maximumSpeed)
        {
            OnStateCompleted.Invoke(this, EventArgs.Empty);
        }
    }

    public void Start()
    {
        _toolbox.SpeedSlider.gameObject.SetActive(true);
        _toolbox.DevilSlider.gameObject.SetActive(false);
    }

    public void Update()
    {
        if (_hero.Boy.AttackSpeed > _hero.MinimumAttackSpeed)
        {
            _hero.Boy.AttackSpeed -= Time.deltaTime * _speedReduceCoefficient;
        }
        _toolbox.SpeedSlider.value = (_hero.Boy.AttackSpeed - _hero.MinimumAttackSpeed) * _sliderCoefficient;
    }

    public void Stun()
    {
        _hero.Boy.AttackSpeed = _hero.MinimumAttackSpeed;
    }
}
