﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.UI;

public class StunAttackState : IHeroAttackState
{
    private readonly ToolboxBehaviour _toolbox;
    private float _cooldownTime = 3f;
    private float _currentValue;
    private readonly float _sliderCoefficient;
    public GameObject Snowball => _toolbox.StunSnowball.gameObject;
    public SimpleActionDelegate OnStateCompleted { get; set; }

    public StunAttackState(ToolboxBehaviour toolbox)
    {
        _toolbox = toolbox;
        _sliderCoefficient = 1 / _cooldownTime;
    }
    public void Tap()
    {
        _currentValue += 0.25f;

        if (_currentValue > _cooldownTime)
        {
            _currentValue = _cooldownTime;
        }
    }

    public void Start()
    {
        _toolbox.DevilSlider.gameObject.SetActive(true);
        _toolbox.SpeedSlider.gameObject.SetActive(false);
        _currentValue = 0.2f;
        foreach (var niceWord in _toolbox.NiceWords)
        {
            niceWord.gameObject.SetActive(false);
        }
        var word = RandomHelper<RawImage>.GetRandomValue(_toolbox.NiceWords);
        word.gameObject.SetActive(true);
        var animator = word.GetComponent<Animator>();
        animator.Play("RawImageFadeOffAnimation");
        GameAnalytics.NewDesignEvent ("speed up mode");
    }

    public void Update()
    {
        _currentValue -= Time.deltaTime * 0.8f;
        if (_currentValue < 0)
        {
            OnStateCompleted.Invoke(this, EventArgs.Empty);
        }

        _toolbox.DevilSlider.value = _currentValue * _sliderCoefficient;
    }

    public void Stun()
    {
        OnStateCompleted.Invoke(this, EventArgs.Empty);
    }
}
