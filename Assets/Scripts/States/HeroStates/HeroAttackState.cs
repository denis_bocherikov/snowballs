﻿using System;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class HeroAttackState : BaseWalkingPlayerState
{
    private Vector2 _startTouchPosition;
    private float _currentRotation;
    private Vector3 _originalRotation;
    private float _maxRotationAngle = 45f;
    private ITarget _selectedTarget;
    private PlayerBehaviour _owner;
    private bool _isCurrentlyAttacking;
    private ToolboxBehaviour _toolbox;
    private readonly HeroBehaviour _hero;
    private readonly bool _isNeedToShowRotateLabel;
    private readonly Vector3 _cameraOffsetPosition;
    private readonly TargetsContainer _targets;
    private readonly Camera _camera;
    private IHeroAttackState _currentState;
    private IHeroAttackState _simpleAttackState;
    private IHeroAttackState _stunAttackState;
    private bool _isTapTipShown;
    private int _tapCount;
    private float _frameRotationValue;

    public override bool IsCanBePaused => true;
    public override bool IsCanBeAttacked => true;

    public HeroAttackState(ToolboxBehaviour toolbox, HeroBehaviour hero, bool isNeedToShowRotateLabel)
    {
        _toolbox = toolbox;
        _hero = hero;
        _isNeedToShowRotateLabel = isNeedToShowRotateLabel;
        _cameraOffsetPosition = _toolbox.CameraOffsetPosition;
        _targets = toolbox.TargetsContainer;
        _camera = toolbox.Camera;
        _simpleAttackState = new SimpleAttackState(_hero, _toolbox, _hero.MinimumAttackSpeed);
        _stunAttackState = new StunAttackState(_toolbox);
        _simpleAttackState.OnStateCompleted += OnFullSpeed;
        _stunAttackState.OnStateCompleted += OnEndOfDevilMode;
        _currentState = _simpleAttackState;
    }

    private void OnFullSpeed(object sender, EventArgs e)
    {
        _currentState = _stunAttackState;
        _currentState.Start();
    }

    private void OnEndOfDevilMode(object sender, EventArgs e)
    {
        _currentState = _simpleAttackState;
        _currentState.Start();
    }

    public override void Act(PlayerBehaviour player)
    {
        _owner = player;
        _owner.Boy.OnThrowEndDelegate += BoyOnThrowEndDelegate;
        _owner.Boy.OnThrowStartDelegate += BoyOnOnThrowStartDelegate;
        _originalRotation = _owner.transform.rotation.eulerAngles;
		_currentRotation = _originalRotation.y;
        if (_isNeedToShowRotateLabel)
        {
            _toolbox.RotateTipLabel.gameObject.SetActive(true);
            _toolbox.RotateTipSlider.gameObject.SetActive(true);
        }
        
        _toolbox.HealthSlider.gameObject.SetActive(true);
        _toolbox.HealthIcon.SetActive(true);
        _toolbox.SpeedIcon.SetActive(true);

        _currentState = _simpleAttackState;
        _currentState.Start();
        _hero.Boy.AttackSpeed = _hero.MinimumAttackSpeed;
    }

    public override void Update(PlayerBehaviour player)
    {
        _camera.transform.position = Vector3.MoveTowards(_camera.transform.position, _hero.CameraPosition.transform.position, Time.deltaTime * 2f);
        _camera.transform.LookAt(_hero.transform.position + _cameraOffsetPosition);

        ProcessTouch(player.gameObject);
        
        _currentState.Update();
    }

    public override void LateUpdate(PlayerBehaviour player)
    {
        if (!_isCurrentlyAttacking)
        {
            _selectedTarget = GetSelectedEnemy(player);
            if (_selectedTarget != null && _selectedTarget.IsCanBeAttacked)
            {
                Attack();
            }
        }

        if (_isTapTipShown && _tapCount > 5)
        {
            _toolbox.TapTip.gameObject.SetActive(false);
        }
        UpdateTargetRingState();
    }

    public override void Stop()
    {
        if (_hero.gameObject != null && _toolbox != null && _toolbox.gameObject != null)
        {
            if (_toolbox.TargetRing.gameObject != null)
            {
                _toolbox.TargetRing.SetActive(false);
            }
        }
        
        _hero.HeroDamageBehaviour.Hide();
        _toolbox.SpeedSlider.gameObject.SetActive(false);
        _toolbox.DevilSlider.gameObject.SetActive(false);
        _toolbox.HealthSlider.gameObject.SetActive(false);
        _toolbox.HealthIcon.SetActive(false);
        _toolbox.SpeedIcon.SetActive(false);

        foreach (var niceWord in _toolbox.NiceWords)
        {
            niceWord.gameObject.SetActive(false);
        }

        base.Stop();
    }

    public override void Stun()
    {
        _currentState.Stun();
    }

    private void UpdateTargetRingState()
    {
        if (_selectedTarget != null && _selectedTarget.IsCanBeAttacked)
        {
            _toolbox.TargetRing.SetActive(true);
            var direction = _camera.transform.position - _selectedTarget.TargetPosition;
            _toolbox.TargetRing.transform.position = _camera.transform.position - direction * 0.2f;
            _toolbox.TargetRing.transform.LookAt(_toolbox.Camera.transform);
        }
        else
        {
            _toolbox.TargetRing.SetActive(false);
        }
    }
    

    protected override void ClearState()
    {
        _owner.Boy.OnThrowEndDelegate -= BoyOnThrowEndDelegate;
        _owner.Boy.OnThrowStartDelegate -= BoyOnOnThrowStartDelegate;
        _owner = null;
    }
    
    private void Attack()
    {
        var throwBehaviour = _owner.GetComponent<SnowballThrowBehaviour>();
        if (!_isCurrentlyAttacking)
        {
            _isCurrentlyAttacking = true;
            _owner.Boy.Attack();
            throwBehaviour.TargetPosition = _selectedTarget.TargetPosition;
        }
    }
    
    void ProcessTouch(GameObject player)
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.touches[0];
            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                ProcessMoveTouch(touch, player);
                if (_selectedTarget != null)
                {
                    _toolbox.RotateTipLabel.gameObject.SetActive(false);
                    _toolbox.RotateTipSlider.gameObject.SetActive(false);
                    if (_isNeedToShowRotateLabel && !_isTapTipShown)
                    {
                        _isTapTipShown = true;
                        _toolbox.TapTip.gameObject.SetActive(true);
                    }
                }
            }
            else if(touch.phase == TouchPhase.Began)
            {
                if (_isTapTipShown)
                {
                    _tapCount++;
                }

                ProcessStartTouch(touch);
            }
        }
    }

    void ProcessStartTouch(Touch touch)
    {
        if (_selectedTarget != null)
        {
            _currentState.Tap();
        }

        _startTouchPosition = touch.position;
    }

    void ProcessMoveTouch(Touch touch, GameObject player)
    {
        //process only rotation
        var deltaX = _startTouchPosition.x - touch.position.x;
        var deltaY = _startTouchPosition.y - touch.position.y;
        if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY))
        {
            _frameRotationValue = Time.deltaTime * 50 * (deltaX * -1 / Screen.width) * 3.5f;
            //_frameRotationValue = (deltaX * -1 / Screen.width) * 4;
            if (_frameRotationValue > 0 && _frameRotationValue < 2f)
            {
                _frameRotationValue = 2f;
            }

            if (_frameRotationValue < 0 && _frameRotationValue > -2f)
            {
                _frameRotationValue = -2f;
            }
            
            
            //Debug.Log(_frameRotationValue);
            _currentRotation += _frameRotationValue;	
            _currentRotation = Mathf.Clamp(_currentRotation, _originalRotation.y - _maxRotationAngle,
                _originalRotation.y + _maxRotationAngle);
            player.transform.eulerAngles = new Vector3(_originalRotation.x, _currentRotation, _originalRotation.z);
        }
    }
    private ITarget GetSelectedEnemy(PlayerBehaviour owner)
    {
        if (_targets != null)
        {
            var targets = _targets.GetItems();
            if (targets.Any())
            {
                var minAngle = CalculateAngleForEnemy(targets.First(), owner);
                var minAngleTarget = targets.First();
//                float secondMinAngle;
//                ITarget secondMinAngleTarget;
                foreach (var target in _targets.GetItems())
                {
                    if (target != null && target.IsCanBeAttacked)
                    {
                        var angle = CalculateAngleForEnemy(target, owner);
                        if (angle < minAngle)
                        {
//                            secondMinAngleTarget = minAngleTarget;
                            minAngle = angle;
                            minAngleTarget = target;
                        }
                    }
                }
                if (minAngle < 12.9f && minAngleTarget.IsCanBeAttacked)
                {
                    return minAngleTarget;
                }
            }
        }

        return null;
    }
	
    private float CalculateAngleForEnemy(ITarget target, PlayerBehaviour player)
    {
        var transform = player.gameObject.transform;
        var targetPosition = new Vector2(target.TargetPosition.x, target.TargetPosition.z);
        var transformPosition = new Vector2(transform.position.x, transform.position.z);
        var forwardPosition = new Vector2(transform.forward.x, transform.forward.z);
		
        var angle = Vector2.Angle(forwardPosition, targetPosition - transformPosition);
        return angle;
    }
    
    private void BoyOnThrowEndDelegate(object sender, EventArgs args)
    {
        _isCurrentlyAttacking = false;
        _selectedTarget = GetSelectedEnemy(_owner);
        if (_selectedTarget == null)
        {
            _owner.Boy.Idle();
        }
        else
        {
            Attack();
        }
    }
    
    private void BoyOnOnThrowStartDelegate(object sender, EventArgs args)
    {
        _selectedTarget = GetSelectedEnemy(_owner);
        var throwBehaviour = _owner.GetComponent<SnowballThrowBehaviour>();
        if (_selectedTarget != null)
        {
            throwBehaviour.HandPosition = _owner.SnowballPosition.transform.position;
            throwBehaviour.Snowball = Instantiate(_currentState.Snowball).GetComponent<ISnowball>();
            throwBehaviour.ThrowSnowball();
        }
        else
        {
            if (!_isStoped)
            {
                _isCurrentlyAttacking = false;
                _owner.Boy.Idle();
            }
        }
    }
}
