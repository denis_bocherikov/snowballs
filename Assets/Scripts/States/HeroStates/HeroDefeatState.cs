﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroDefeatState : BaseWalkingPlayerState
{
    private readonly HeroBehaviour _hero;
    private readonly ToolboxBehaviour _toolbox;

    public HeroDefeatState(HeroBehaviour hero, ToolboxBehaviour toolbox)
    {
        _hero = hero;
        _toolbox = toolbox;
    }
    public override bool IsCanBePaused => false;
    protected override void ClearState()
    {
    }

    public override void Act(PlayerBehaviour player)
    {
        _toolbox.TargetRing.SetActive(false);
        _hero.transform.eulerAngles = new Vector3(_hero.transform.eulerAngles.x, _hero.transform.eulerAngles.y + 180, _hero.transform.eulerAngles.z);
        _hero.Boy.Defeat();
        _toolbox.Fail.gameObject.SetActive(true);
    }

    public override void Update(PlayerBehaviour player)
    {
        _toolbox.Camera.transform.position = Vector3.MoveTowards(_toolbox.Camera.transform.position,
            _hero.transform.TransformPoint(_toolbox.VictoryCameraOffsetPosition), Time.deltaTime * 0.2f);
        _toolbox.Camera.transform.LookAt(player.transform);
    }
}
