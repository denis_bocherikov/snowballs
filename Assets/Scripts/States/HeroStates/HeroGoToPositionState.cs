﻿using System;
using UnityEngine;

public class HeroGoToPositionState : GoToPositionState
{
    private readonly ToolboxBehaviour _toolbox;
    private readonly HeroBehaviour _hero;
    private Camera _camera;
    private Vector3 _lookPosition;

    public HeroGoToPositionState(HeroAttackPositionBehaviour position, ITarget target, ToolboxBehaviour toolbox, HeroBehaviour hero) : 
        base(position.transform.position, target)
    {
        _toolbox = toolbox;
        _hero = hero;
        _camera = toolbox.Camera;
        _lookPosition = position.LookAtPosition.transform.position;
    }

    public override void Update(PlayerBehaviour player)
    {
        base.Update(player);
        //if (_distanceToPosition > 1f)
        {
            var cameraPosition = _hero.transform.position - _lookPosition;
            _camera.transform.position = Vector3.MoveTowards(_camera.transform.position,
                _hero.transform.position + cameraPosition.normalized * 2f + _toolbox.CameraOffsetPosition,
                Time.deltaTime * 2f);
            _camera.transform.LookAt(_lookPosition);
        }
    }

    protected override void Complete()
    {
        _hero.OnArriveToPosition?.Invoke(this, EventArgs.Empty);
        base.Complete();
    }
}