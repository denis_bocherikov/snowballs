﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTargetState : BaseWalkingPlayerState
{
    private readonly Vector3 _position;

    public LookAtTargetState(Vector3 position)
    {
        _position = position;
    }

    public override bool IsCanBePaused => false;
    protected override void ClearState()
    {
        
    }

    public override void Act(PlayerBehaviour player)
    {
        player.transform.LookAt(_position);
        Complete();
    }

    public override void Update(PlayerBehaviour player)
    {
    }
}
